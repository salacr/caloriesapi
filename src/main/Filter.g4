grammar Filter;

filter: boolexpression EOF;

boolexpression
    : LPAREN boolexpression RPAREN   # ParenExp
    | boolexpression AND boolexpression  # AndBlock
    | boolexpression OR boolexpression   # OrBlock
    | boolterm                       # AtomExp
    ;

boolterm
    : LPAREN boolterm RPAREN # BoolTermParenthesisBlok
    | numberatom (EQ | NE | GT | LT) numberatom # NumberAtomBlok
    | stringlikeatom (EQ | NE | GT | LT) stringlikeatom # StringLikeAtomBlok
    | boolatom (EQ | NE ) boolatom # BoolAtomBlok
    ;

stringlikeatom
    : date_column
    | time_column
    | string_column
    | uuid_column
    | STRING
    ;

numberatom
    : NUMBER
    | numeric_column
    ;


boolatom
    : BOOL
    | bool_column
    ;

BOOL: 'true' | 'false' ;



bool_column: 'user.isLocked' | 'user.emailVerified' | 'meal.isLessCaloriesThenExpected';
time_column: 'meal.time';
date_column: 'meal.date';
numeric_column: 'meal.calories' | 'user.badPasswordsInRow' | 'user.calloriesPerDay';
uuid_column: 'meal.id' | 'meal.userid' | 'user.id' ;
string_column: 'meal.id' | 'meal.userid' | 'meal.text' | 'user.firstName' | 'user.lastName' | 'user.email' | 'user.role';


EQ : 'eq';
NE : 'ne';
GT : 'gt';
LT : 'lt';



STRING: '\'' ( Escape | ~('\'' | '\\' | '\n' | '\r') ) + '\'';
fragment Escape : '\\' ( '\'' | '\\' );


NUMBER: ([+-])?DIGITS(.DIGITS)?;
DIGITS : [0-9]+;

AND : 'AND' | 'and';
OR : 'OR' | 'or';

LPAREN : '(';
RPAREN : ')';

WS: [ \t\r\n]+ -> skip;