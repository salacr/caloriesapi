CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table user_login
(
  id                       uuid NOT NULL
    constraint user_login_pkey
    primary key,
  bad_login_atempts_in_row integer CHECK (bad_login_atempts_in_row >= 0) NOT NULL DEFAULT 0,
  calories_per_day         numeric(19, 2) CHECK (calories_per_day >= 0) NOT NULL,
  email                    varchar(255)
    constraint user_login_unique_email
    unique NOT NULL,
  email_verified           boolean NOT NULL DEFAULT false,
  first_name               varchar(255) NOT NULL,
  is_locked                boolean NOT NULL DEFAULT false,
  last_name                varchar(255) NOT NULL,
  password                 varchar(255) NOT NULL,
  role                     varchar(255) NOT NULL
);

create table refresh_token
(
  id            uuid NOT NULL
    constraint refresh_token_pkey
    primary key,
  user_login_id uuid NOT NULL
    constraint fk_refresh_token_user_login_id
    references user_login  ON DELETE CASCADE
);


-- auto-generated definition
create table meal
(
  id                    uuid NOT NULL
    constraint meal_pkey
    primary key,
  calories              numeric(19, 2) NOT NULL,
  date                  date NOT NULL,
  text                  varchar(255) NOT NULL,
  time                  time NOT NULL,
  user_login_id         uuid NOT NULL
    constraint fk_meal_user_login_id
    references user_login ON DELETE CASCADE
);

create table consumed_calories_per_day
(
  user_login_id         uuid NOT NULL
    constraint fk_consumed_calories_per_day_user_login_id
    references user_login  ON DELETE CASCADE,
  date date NOT NULL,
  calories NUMERIC(19, 2) CHECK (calories >= 0) NOT NULL DEFAULT 0,
  primary key (user_login_id, date)
);

-- auto-generated definition
create table lost_password_token
(
  id            uuid NOT NULL
    constraint lost_password_token_pkey
    primary key,
  created       timestamp NOT NULL DEFAULT NOW(),
  user_login_id uuid
    constraint uk_lost_password_token_user_login_id
    unique
    constraint fk_lost_password_token_user_login_id
    references user_login  ON DELETE CASCADE
);

-- auto-generated definition
create table invitation_token
(
  id    uuid NOT NULL
    constraint invitation_token_pkey
    primary key,
  created       timestamp NOT NULL DEFAULT NOW(),
  email varchar(255) NOT NULL
    constraint uk_invitation_token_email
    unique
);

-- auto-generated definition
create table email_verification_token
(
  id            uuid NOT NULL
    constraint email_verification_token_pkey
    primary key,
  created       timestamp NOT NULL DEFAULT NOW(),
  user_login_id uuid NOT NULL
    constraint uk_email_verification_token_user_login_id
    unique
    constraint fk_email_verification_token_user_login_id
    references user_login  ON DELETE CASCADE
);

CREATE OR REPLACE FUNCTION check_daily_callories() RETURNS trigger AS $BODY$
BEGIN
  IF (TG_OP = 'UPDATE' AND OLD.calories IS NOT DISTINCT FROM NEW.calories) THEN
    RETURN NULL;
  END IF;



  IF (TG_OP = 'DELETE' OR TG_OP = 'UPDATE') THEN
    UPDATE consumed_calories_per_day SET calories = calories - OLD.calories
    WHERE user_login_id = OLD.user_login_id AND date = OLD.date;
  END IF;

  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN

    INSERT INTO consumed_calories_per_day(calories, date, user_login_id)
    VALUES (NEW.calories, NEW.date, NEW.user_login_id)
    ON CONFLICT (user_login_id, date )
    DO UPDATE SET calories = consumed_calories_per_day.calories + EXCLUDED.calories;

  END IF;


  RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_daily_callories ON "public"."meal";

CREATE TRIGGER update_daily_callories AFTER INSERT OR UPDATE OR DELETE ON "public"."meal" FOR EACH ROW
EXECUTE PROCEDURE check_daily_callories();

CREATE VIEW meal_with_consumed_calories_per_day AS
SELECT
  meal.id AS id,
  meal.calories AS calories,
  meal.date AS "date",
  meal.time AS "time",
  meal.text AS text,
  meal.user_login_id AS user_login_id,
  consumed_calories_per_day.calories AS consumed_calories_per_day,
  consumed_calories_per_day.calories < user_login.calories_per_day AS is_less_then_expected
  FROM meal
  INNER JOIN user_login ON
    user_login.id = meal.user_login_id
  INNER JOIN consumed_calories_per_day ON
    consumed_calories_per_day.user_login_id = meal.user_login_id
    AND consumed_calories_per_day.date = meal.date;

-- Default data mainly for testing, password are really weak to production
-- ROLE_USER: Login: user@example.com , Password: user@example.com
-- ROLE_MANAGER: Login: manager@example.com , Password: manager@example.com
-- ROLE_ADMIN: Login: admin@example.com , Password: admin@example.com

INSERT INTO "user_login" ("id", "first_name", "last_name", "email", "password", "role", "email_verified", "bad_login_atempts_in_row", "is_locked", "calories_per_day") VALUES
('a212df05-a3c7-4335-bb84-4fe8db0a1498',	'FooUser', 'BarUser', 'user@example.com',	'$2a$12$aiT90kOhHtQNFpCCl9BaReRRuACxqPZ6dZ..n93mWASN.jQG11.CC',	'ROLE_USER', true, 0, false, 1000),
('de600d24-ffea-42d3-bf13-1e911fcfecea',	'FooManager', 'BarManager', 'manager@example.com',	'$2a$12$NkCRF5zhEaMU8e23sbUki.LCV7pq1OZO8MMpR0gM89jYuMowLuisa',	'ROLE_MANAGER', true, 0, false, 2000),
('5e1398d9-cb98-4db4-8d16-547bea8874b9',	'FooAdmin', 'BarAdmin', 'admin@example.com',	'$2a$12$MOP5ci4rpW5hZwkYAAwlHel/7a8eihekL4FZlrqMTzUwZBCVTJDcC',	'ROLE_ADMIN', true, 0, false, 3000);

INSERT INTO "meal" ("id", "calories", "date", "text", "time", "user_login_id") VALUES

-- user
('41c99109-2c28-49e2-9e20-ad8a7c029643', 10, '2018-01-01', 'eggs', '10:00:00', 'a212df05-a3c7-4335-bb84-4fe8db0a1498'),
('513c9024-3c98-4818-8e00-37f052e2517e', 20, '2018-01-02', 'ham', '11:00:00', 'a212df05-a3c7-4335-bb84-4fe8db0a1498'),
('ce79ec58-0ae9-485c-ad52-97a793a21831', 30, '2018-01-03', 'ham and eggs', '11:15:00', 'a212df05-a3c7-4335-bb84-4fe8db0a1498'),

-- manager
('eb33c72d-26f5-404b-b99d-0f5ee796345d', 30, '2018-01-04', 'cola', '11:30:00', 'de600d24-ffea-42d3-bf13-1e911fcfecea'),
('eb33c72d-26f5-404b-b99d-0f5ee796346d', 60, '2018-01-05', '2 cola', '11:35:00', 'de600d24-ffea-42d3-bf13-1e911fcfecea');
