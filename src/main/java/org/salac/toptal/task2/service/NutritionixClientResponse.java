package org.salac.toptal.task2.service;

public class NutritionixClientResponse{

    private Food[] foods;

    public NutritionixClientResponse() {
    }

    public Food[] getFoods() {
        return foods;
    }

    public void setFoods(Food[] foods) {
        this.foods = foods;
    }
}