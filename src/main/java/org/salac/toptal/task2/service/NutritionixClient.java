package org.salac.toptal.task2.service;

import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Collections;

/**
 * REST client for nutritionix.com
 */
public class NutritionixClient implements CaloriesCalculator {

    private final RestTemplate restTemplate;

    /**
     * Constructor accepts credentials provided by nutritionix.com
     * @param appId
     * @param appKey
     */
    public NutritionixClient(String appId, String appKey) {
        this.restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        restTemplate.setInterceptors(Collections.singletonList((httpRequest, body, clientHttpRequestExecution) -> {
            httpRequest.getHeaders().add("x-app-id", appId);
            httpRequest.getHeaders().add("x-app-key", appKey);
            httpRequest.getHeaders().setContentType(MediaType.APPLICATION_JSON);

            return clientHttpRequestExecution.execute(httpRequest, body);
        }));
    }

    @Override
    public BigDecimal getCalories(String meal) {

        var response = restTemplate.postForEntity("https://trackapi.nutritionix.com/v2/natural/nutrients", new NutritionixClientRequest(meal), NutritionixClientResponse.class);

        var foods = response.getBody().getFoods();
        BigDecimal sumOfCallories = BigDecimal.ZERO;
        for(Food food : foods)
        {
            sumOfCallories = sumOfCallories.add(food.getCalories());
        }

        return sumOfCallories;
    }

}
