package org.salac.toptal.task2.service;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Food{

    @JsonProperty("food_name")
    private String foodName;

    @JsonProperty("serving_qty")
    private BigDecimal servingQty;

    @JsonProperty("serving_unit")
    private String servingUnit;

    @JsonProperty("serving_weight_grams")
    private BigDecimal servingWeightGrams;

    @JsonProperty("nf_calories")
    private BigDecimal calories;

    public BigDecimal getCalories() {
        return calories;
    }

    public Food() {
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public BigDecimal getServingQty() {
        return servingQty;
    }

    public void setServingQty(BigDecimal servingQty) {
        this.servingQty = servingQty;
    }

    public String getServingUnit() {
        return servingUnit;
    }

    public void setServingUnit(String servingUnit) {
        this.servingUnit = servingUnit;
    }

    public BigDecimal getServingWeightGrams() {
        return servingWeightGrams;
    }

    public void setServingWeightGrams(BigDecimal servingWeightGrams) {
        this.servingWeightGrams = servingWeightGrams;
    }

    public void setCalories(BigDecimal calories) {
        this.calories = calories;
    }
}