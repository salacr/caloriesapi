package org.salac.toptal.task2.service;

public class NutritionixClientRequest{
    private final String query;

    public NutritionixClientRequest(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }
}
