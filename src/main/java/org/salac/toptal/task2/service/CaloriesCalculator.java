package org.salac.toptal.task2.service;

import java.math.BigDecimal;

public interface CaloriesCalculator {

    /**
     * return calories in given meal
     * @param meal
     * @return number of calories
     */
    BigDecimal getCalories(String meal);

}
