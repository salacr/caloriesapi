package org.salac.toptal.task2;

import org.salac.toptal.task2.exception.ForbiddenException;
import org.salac.toptal.task2.exception.InvalidDataException;
import org.salac.toptal.task2.exception.MissingEntityException;
import org.salac.toptal.task2.exception.UserException;
import org.salac.toptal.task2.filterParser.FilterParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Handler which hanles various exception thrown by application
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ InvalidDataException.class })
    public ResponseEntity<Object> handleInvalidDataException(InvalidDataException ex, WebRequest request) {
        return ResponseEntity.badRequest().body(ex.getFailures());
    }

    @ExceptionHandler({ ForbiddenException.class, AccessDeniedException.class})
    public ResponseEntity<Object> handleMissingEntityException(ForbiddenException ex, WebRequest request) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(ex.getMessage());
    }

    @ExceptionHandler({ MissingEntityException.class })
    public ResponseEntity<String> handleMissingEntityException(MissingEntityException ex, WebRequest request) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Requested entity can't be found. Probably wrong ID");
    }

    @ExceptionHandler({ UserException.class })
    public ResponseEntity<String> handleUserError(UserException ex, WebRequest request) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler({ FilterParseException.class })
    public ResponseEntity<String> handleUserError(FilterParseException ex, WebRequest request) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error");
    }
}
