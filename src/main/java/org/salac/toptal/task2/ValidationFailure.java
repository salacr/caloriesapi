package org.salac.toptal.task2;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Holds human readable errors shown to user when validation fails
 */
public class ValidationFailure {

    private final Set<HumanReadableFieldError> fieldErrors = new HashSet<>();

    private final Set<HumanReadableError> objectError = new HashSet<>();

    /**
     * Used by jackson
     */
    public ValidationFailure() {
    }

    public ValidationFailure(Collection<FieldError> fieldErrors) {
        this.fieldErrors.addAll(fieldErrors.stream()
                .map(error -> new HumanReadableFieldError(error.getField(), error.getDefaultMessage()))
                .collect(Collectors.toSet()));
    }

    public ValidationFailure(List<ObjectError> globalErrors, List<FieldError> fieldErrors) {
        this(fieldErrors);
        this.objectError.addAll(globalErrors.stream()
                .map(error -> new HumanReadableError(error.getDefaultMessage()))
                .collect(Collectors.toSet()));
    }

    @Override
    public String toString() {
        return "ValidationFailure{" +
                "fieldErrors=" + fieldErrors +
                ", objectError=" + objectError +
                '}';
    }

    public Set<HumanReadableFieldError> getFieldErrors() {
        return fieldErrors;
    }

    public Set<HumanReadableError> getObjectError() {
        return objectError;
    }
}
