package org.salac.toptal.task2.constraint;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation which checks that date/time of the meal isn't set in future
 */
@Constraint(validatedBy =  {MealIsntInFutureValidator.class})
@Retention(RUNTIME)
public @interface MealIsntInFuture {

    String message() default "You can't set future date";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
