package org.salac.toptal.task2.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validates password strength by rules defined in annotation
 */
public class PasswordStrengthValidator implements ConstraintValidator<PasswordStrength, String> {

    private int minLength;

    @Override
    public void initialize(PasswordStrength constraintAnnotation) {
        minLength = constraintAnnotation.minLength();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null)
        {
            return true;
        }

        // check length
        if (value.length() < minLength)
        {
            return false;
        }

        // at least one lower case letter
        if (!value.matches(".*[a-z].*"))
        {
            return false;
        }

        // at least one upper case letter
        if (!value.matches(".*[A-Z].*"))
        {
            return false;
        }

        // at least one number
        return value.matches(".*[0-9].*");
    }
}