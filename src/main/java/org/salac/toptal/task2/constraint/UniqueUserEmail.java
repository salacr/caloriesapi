package org.salac.toptal.task2.constraint;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation which marks a field as a unique user email.
 */
@Constraint(validatedBy =  {UniqueUserEmailValidator.class})
@Retention(RUNTIME)
public @interface UniqueUserEmail {

    String message() default "UserLogin with this e-mail already exists.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
