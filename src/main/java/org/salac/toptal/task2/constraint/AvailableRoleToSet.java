package org.salac.toptal.task2.constraint;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation which is used to validate role which can be set during promotion
 */
@Constraint(validatedBy =  {AvailableRoleToSetValidator.class})
@Retention(RUNTIME)
public @interface AvailableRoleToSet {

    String message() default "You aren't eligible to set this role";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
