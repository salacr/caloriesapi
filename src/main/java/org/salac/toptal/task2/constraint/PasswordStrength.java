package org.salac.toptal.task2.constraint;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation used for validation of password strength
 */
@Constraint(validatedBy =  {PasswordStrengthValidator.class})
@Retention(RUNTIME)
public @interface PasswordStrength {

    String message() default "Password must contain at least 8 characters, including 1 uppercase letter, 1 lowercase letter, and 1 number.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return Minimum password length
     */
    int minLength() default 6;
}
