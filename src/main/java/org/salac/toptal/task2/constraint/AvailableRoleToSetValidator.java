package org.salac.toptal.task2.constraint;

import org.salac.toptal.task2.auth.AuthFacade;
import org.salac.toptal.task2.dto.Role;
import org.salac.toptal.task2.dto.UserLogin;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Checks that user is eligible to set given role during promotion
 */
public class AvailableRoleToSetValidator implements ConstraintValidator<AvailableRoleToSet, Role> {

    private AuthFacade authFacede;

    @Autowired
    public void setUserRepository(AuthFacade authFacede) {
        this.authFacede = authFacede;
    }

    @Override
    public boolean isValid(Role role, ConstraintValidatorContext context) {
        var currentUserRole = ((UserLogin) authFacede.getAuthentication().getPrincipal()).getRole();
        if (currentUserRole == Role.ROLE_USER)
        {
            return false;
        }
        if (currentUserRole == Role.ROLE_MANAGER)
        {
            return role == Role.ROLE_MANAGER;
        }
        else
        {
            return true;
        }
    }
}