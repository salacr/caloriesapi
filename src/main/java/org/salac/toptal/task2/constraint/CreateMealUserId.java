package org.salac.toptal.task2.constraint;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation which checks if user is eligible to create meal for particular user
 */
@Constraint(validatedBy =  {CreateMealUserIdValidator.class})
@Retention(RUNTIME)
public @interface CreateMealUserId {

    String message() default "You aren't eligible to create meal for that user";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
