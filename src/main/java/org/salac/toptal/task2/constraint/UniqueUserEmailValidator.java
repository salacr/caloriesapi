package org.salac.toptal.task2.constraint;

import org.salac.toptal.task2.dto.repository.UserLoginRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Checks that the user email is unique
 */
public class UniqueUserEmailValidator implements ConstraintValidator<UniqueUserEmail, String> {

    private UserLoginRepository users;

    @Autowired
    public void setUserRepository(UserLoginRepository users) {
        this.users = users;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || !users.findByEmail(value).isPresent();
    }
}