package org.salac.toptal.task2.constraint;

import org.salac.toptal.task2.auth.AuthFacade;
import org.salac.toptal.task2.dto.Role;
import org.salac.toptal.task2.dto.UserLogin;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

/**
 * Validator which checks if user is eligible to create meal for particular user
 */
public class CreateMealUserIdValidator implements ConstraintValidator<CreateMealUserId, UUID> {

    private AuthFacade authFacede;

    @Autowired
    public void setUserRepository(AuthFacade authFacede) {
        this.authFacede = authFacede;
    }

    @Override
    public boolean isValid(UUID value, ConstraintValidatorContext context) {
        if (value == null)
        {
            return true;
        }
        var userLogin = (UserLogin) authFacede.getAuthentication().getPrincipal();
        return userLogin.getId().equals(value) || Role.ROLE_ADMIN == userLogin.getRole();
    }
}