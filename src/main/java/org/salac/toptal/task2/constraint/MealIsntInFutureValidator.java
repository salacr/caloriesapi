package org.salac.toptal.task2.constraint;

import org.salac.toptal.task2.controller.meal.MealRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

/**
 * Validator which checks that date/time of the meal isn't set in future
 */
public class MealIsntInFutureValidator implements ConstraintValidator<MealIsntInFuture, MealRequest> {

    @Override
    public boolean isValid(MealRequest value, ConstraintValidatorContext context) {
        if (value.getDate() == null || value.getTime() == null)
        {
            return true;
        }

        // TODO: we can improve this by taking time-zone in effect
        // Don't be a pedant we aren't bank application
        var currentDateTime = LocalDateTime.now().plusMinutes(10);

        var requestDateTime = LocalDateTime.of(value.getDate(), value.getTime());

        return currentDateTime.isAfter(requestDateTime);
    }
}