package org.salac.toptal.task2.dto;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class RefreshToken {

    @Id
    @Column
    private final UUID id = UUID.randomUUID();

    @ManyToOne
    @JoinColumn
    private UserLogin userLogin;

    public RefreshToken() {
    }

    public RefreshToken(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public UUID getId() {
        return id;
    }

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }
}