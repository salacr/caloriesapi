package org.salac.toptal.task2.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class InvitationToken {

    @Id
    @Column
    private final UUID id = UUID.randomUUID();

    @Column(unique = true)
    private String email;

    @Column
    private LocalDateTime created = LocalDateTime.now();

    public InvitationToken() {
    }

    public InvitationToken(String email) {
        this.email = email;
    }

    public UUID getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
