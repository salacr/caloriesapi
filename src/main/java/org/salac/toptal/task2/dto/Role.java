package org.salac.toptal.task2.dto;

public enum Role {
    ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN
}
