package org.salac.toptal.task2.dto.service;

import org.salac.toptal.task2.SimpleQueryBuilder;
import org.salac.toptal.task2.dto.Meal;
import org.salac.toptal.task2.dto.Role;
import org.salac.toptal.task2.dto.UserLogin;
import org.salac.toptal.task2.dto.repository.MealRepository;
import org.salac.toptal.task2.exception.ForbiddenException;
import org.salac.toptal.task2.exception.MissingEntityException;
import org.salac.toptal.task2.exception.UserException;
import org.salac.toptal.task2.filterParser.DbColumnsConfig;
import org.salac.toptal.task2.filterParser.FilterInterpret;
import org.salac.toptal.task2.filterParser.QueryType;
import org.salac.toptal.task2.filterParser.SQLFragment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.UUID;

@Component
public class MealService {

    private final MealRepository mealRepository;

    private final JdbcTemplate jdbc;

    private final DbColumnsConfig columnConfig = new DbColumnsConfig();

    public MealService(@Autowired MealRepository mealRepository, @Autowired JdbcTemplate jdbc) {
        this.mealRepository = mealRepository;
        this.jdbc = jdbc;
    }

    public MealListResponse list(UserLogin logedUser, Pageable pageable, boolean checkNext, boolean shouldCountAllRows, String mealFilter) {

        var sort = pageable.getSortOr(new Sort(Sort.Direction.ASC, "meal.date"));
        var size = pageable.getPageSize();

        var queryBuilder = SimpleQueryBuilder.select("*")
                .from("meal_with_consumed_calories_per_day AS meal")
                .setOffset((int) pageable.getOffset())
                .setLimit(checkNext ? size + 1 : size);
        sort.forEach(order -> {
           var columnInfo = columnConfig.get(order.getProperty());
           if (columnInfo == null)
           {
                var eligibleColumns = String.join(", ", columnConfig.getListOfEligibleFieldsForQueryType(QueryType.MEAL));
                throw new UserException("You can't sort by this column, column in this reports are:" + eligibleColumns);
           }
           queryBuilder.addOrderBy(columnInfo.getDbRepresentation(), order.isAscending());
        });

        var filterInterpret = new FilterInterpret();
        var filter = filterInterpret.interpret(mealFilter, QueryType.MEAL);

        if (!filter.getSql().isEmpty()) {
            queryBuilder.andWhere(filter.getSql(), filter.getBinds().toArray());
        }

        if (logedUser.getRole() != Role.ROLE_ADMIN)
        {
            queryBuilder.andWhere("meal.user_login_id = ?", logedUser.getId());
        }

        SQLFragment query = queryBuilder.toSqlFragment();
        var result = jdbc.query(query.getSql(), query.getBinds().toArray(), (resultSet, row) -> {
            var meal =  new MealRow();
            meal.setText(resultSet.getString("text"));
            meal.setCalories(resultSet.getBigDecimal("calories"));
            meal.setConsumedCaloriesPerDay(resultSet.getBigDecimal("consumed_calories_per_day"));
            meal.setDate(LocalDate.parse(resultSet.getString("date")));
            meal.setTime(LocalTime.parse(resultSet.getString("time")));
            meal.setUser(UUID.fromString(resultSet.getString("user_login_id")));
            meal.setId(UUID.fromString(resultSet.getString("id")));
            meal.setLessThenExpected(resultSet.getBoolean("is_less_then_expected"));
            return meal;
        }).toArray(new MealRow[]{});

        Long count = null;
        if (shouldCountAllRows)
        {
            var countQuery = queryBuilder.toCountQueryBuilder().toSqlFragment();
            count = jdbc.queryForObject(countQuery.getSql(), countQuery.getBinds().toArray(), Long.class);
        }

        return new MealListResponse(
                Arrays.copyOfRange(result, 0, Math.min(result.length, size)),
                checkNext ? result.length == size + 1 : null,
                count
        );
    }

    public Meal getByUserLoginAndId(UserLogin user, UUID id) {

        var meal = mealRepository.findById(id).orElseThrow(MissingEntityException::new);
        if (user.getRole() != Role.ROLE_ADMIN && !meal.getUserLogin().getId().equals(user.getId()))
        {
            throw new ForbiddenException();
        }
        return meal;
    }

    public void save(Meal meal)
    {
        mealRepository.saveAndFlush(meal);
    }

    public void delete(Meal meal)
    {
        mealRepository.delete(meal);
        mealRepository.flush();
    }
}
