package org.salac.toptal.task2.dto.service;


import org.salac.toptal.task2.dto.LostPasswordToken;
import org.salac.toptal.task2.dto.UserLogin;
import org.salac.toptal.task2.dto.repository.LostPasswordTokenRepository;
import org.salac.toptal.task2.exception.MissingEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.UUID;

@Component
public class LostPasswordService {

    private final LostPasswordTokenRepository lostPasswordTokenRepository;

    private final EntityManager entityManager;

    public LostPasswordService(@Autowired EntityManager entityManager, @Autowired LostPasswordTokenRepository lostPasswordTokenRepository) {
        this.lostPasswordTokenRepository = lostPasswordTokenRepository;
        this.entityManager = entityManager;
    }

    @Transactional
    public LostPasswordToken getNewLostPasswordToken(UserLogin user) {

        if (user.getLostPasswordToken() != null) {
            entityManager.remove(user.getLostPasswordToken());
        }
        var lostPasswordToken = new LostPasswordToken(user);
        entityManager.persist(lostPasswordToken);
        entityManager.flush();
        return lostPasswordToken;

    }

    public LostPasswordToken getById(UUID id)
    {
        return lostPasswordTokenRepository.findById(id).orElseThrow(MissingEntityException::new);
    }
}
