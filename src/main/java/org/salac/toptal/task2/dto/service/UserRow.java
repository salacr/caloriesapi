package org.salac.toptal.task2.dto.service;

import org.salac.toptal.task2.dto.Role;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class UserRow {

    private UUID id;

    private String email;

    private String firstName;

    private String lastName;

    private Role role;

    private boolean isLocked = false;

    private boolean emailVerified = false;

    private int badLoginAtemptsInRow = 0;

    private BigDecimal caloriesPerDay;

    public UserRow() {
    }

    public UserRow(UUID id, String firstName, String lastName, String email, Role role, boolean emailVerified, int badLoginAtemptsInRow, boolean isLocked, BigDecimal caloriesPerDay) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.isLocked = isLocked;
        this.emailVerified = emailVerified;
        this.badLoginAtemptsInRow = badLoginAtemptsInRow;
        this.caloriesPerDay = caloriesPerDay;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public int getBadLoginAtemptsInRow() {
        return badLoginAtemptsInRow;
    }

    public void setBadLoginAtemptsInRow(int badLoginAtemptsInRow) {
        this.badLoginAtemptsInRow = badLoginAtemptsInRow;
    }

    public BigDecimal getCaloriesPerDay() {
        return caloriesPerDay;
    }

    public void setCaloriesPerDay(BigDecimal caloriesPerDay) {
        this.caloriesPerDay = caloriesPerDay;
    }

    @Override
    public String toString() {
        return "UserRow{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", role=" + role +
                ", isLocked=" + isLocked +
                ", emailVerified=" + emailVerified +
                ", badLoginAtemptsInRow=" + badLoginAtemptsInRow +
                ", caloriesPerDay=" + caloriesPerDay +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRow userRow = (UserRow) o;
        return isLocked == userRow.isLocked &&
                emailVerified == userRow.emailVerified &&
                badLoginAtemptsInRow == userRow.badLoginAtemptsInRow &&
                Objects.equals(id, userRow.id) &&
                Objects.equals(email, userRow.email) &&
                Objects.equals(firstName, userRow.firstName) &&
                Objects.equals(lastName, userRow.lastName) &&
                role == userRow.role &&
                Objects.equals(caloriesPerDay, userRow.caloriesPerDay);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
