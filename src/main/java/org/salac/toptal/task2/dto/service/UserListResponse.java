package org.salac.toptal.task2.dto.service;

public class UserListResponse {

    private UserRow[] data;

    private Boolean next;

    private Long count;

    public UserListResponse() {
    }

    public UserListResponse(UserRow[] data, Boolean hasNext, Long count) {
        this.data = data;
        this.next = hasNext;
        this.count = count;
    }

    public UserRow[] getData() {
        return data;
    }

    public Boolean isNext() {
        return next;
    }

    public Long getCount() {
        return count;
    }
}
