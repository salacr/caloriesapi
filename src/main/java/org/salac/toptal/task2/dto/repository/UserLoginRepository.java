package org.salac.toptal.task2.dto.repository;

import org.salac.toptal.task2.dto.UserLogin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserLoginRepository extends JpaRepository<UserLogin, UUID> {

    Optional<UserLogin> findByEmail(String email);

    UserLogin getByEmail(String email);

}
