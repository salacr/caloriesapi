package org.salac.toptal.task2.dto.repository;

import org.salac.toptal.task2.dto.Meal;
import org.salac.toptal.task2.dto.UserLogin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface MealRepository extends JpaRepository<Meal, UUID> {

    Optional<Meal> findByUserLoginAndId(UserLogin userLogin, UUID uuid);
}
