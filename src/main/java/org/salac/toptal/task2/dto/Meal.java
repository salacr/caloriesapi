package org.salac.toptal.task2.dto;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

@Entity
public class Meal {

    @Id
    @Column
    private final UUID id = UUID.randomUUID();


    @ManyToOne
    @JoinColumn(name="user_login_id")
    private UserLogin userLogin;

    @Column
    private LocalDate date;

    @Column
    private LocalTime time;

    @Column
    private String text;

    @Column
    private BigDecimal calories;

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public UUID getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public BigDecimal getCalories() {
        return calories;
    }

    public void setCalories(BigDecimal calories) {
        this.calories = calories;
    }

}
