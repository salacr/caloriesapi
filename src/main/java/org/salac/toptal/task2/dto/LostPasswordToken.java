package org.salac.toptal.task2.dto;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class LostPasswordToken {

    @Id
    @Column
    private final UUID id = UUID.randomUUID();

    @OneToOne
    @JoinColumn(unique = true)
    private UserLogin userLogin;

    @Column
    private LocalDateTime created = LocalDateTime.now();

    public LostPasswordToken() {
    }

    public LostPasswordToken(UserLogin userLogin) {
        this.userLogin = userLogin;
        userLogin.setLostPasswordToken(this);
    }

    public UUID getId() {
        return id;
    }

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }
}
