package org.salac.toptal.task2.dto.service;

import org.salac.toptal.task2.SimpleQueryBuilder;
import org.salac.toptal.task2.dto.InvitationToken;
import org.salac.toptal.task2.dto.RefreshToken;
import org.salac.toptal.task2.dto.Role;
import org.salac.toptal.task2.dto.UserLogin;
import org.salac.toptal.task2.dto.repository.UserLoginRepository;
import org.salac.toptal.task2.exception.MissingEntityException;
import org.salac.toptal.task2.exception.UserException;
import org.salac.toptal.task2.filterParser.DbColumnsConfig;
import org.salac.toptal.task2.filterParser.FilterInterpret;
import org.salac.toptal.task2.filterParser.QueryType;
import org.salac.toptal.task2.filterParser.SQLFragment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserService {

    private final UserLoginRepository users;

    private final JdbcTemplate jdbc;

    private final DbColumnsConfig columnConfig = new DbColumnsConfig();

    private final EntityManager entityManager;

    public UserService(@Autowired EntityManager entityManager, @Autowired UserLoginRepository users, @Autowired JdbcTemplate jdbc) {
        this.users = users;
        this.jdbc = jdbc;
        this.entityManager = entityManager;
    }

    public UserListResponse list(UserLogin logedUser, Pageable pageable, boolean checkNext, boolean shouldCountAllRows, String userFilter) {

        var sort = pageable.getSortOr(new Sort(Sort.Direction.ASC, "user.email"));
        var size = pageable.getPageSize();

        var queryBuilder = SimpleQueryBuilder.select("*")
                .from("user_login")
                .setOffset((int) pageable.getOffset())
                .setLimit(checkNext ? size + 1 : size);
        sort.forEach(order -> {
            var columnInfo = columnConfig.get(order.getProperty());
            if (columnInfo == null)
            {
                var eligibleColumns = String.join(", ", columnConfig.getListOfEligibleFieldsForQueryType(QueryType.USER));
                throw new UserException("You can't sort by this column, column in this reports are:" + eligibleColumns);
            }
            queryBuilder.addOrderBy(columnInfo.getDbRepresentation(), order.isAscending());
        });

        var filterInterpret = new FilterInterpret();
        var filter = filterInterpret.interpret(userFilter, QueryType.USER);

        if (!filter.getSql().isEmpty()) {
            queryBuilder.andWhere(filter.getSql(), filter.getBinds().toArray());
        }

        if (logedUser.getRole() == Role.ROLE_USER)
        {
            queryBuilder.andWhere("user_login.id = ?", logedUser.getId());
        }

        SQLFragment query = queryBuilder.toSqlFragment();
        var result = jdbc.query(query.getSql(), query.getBinds().toArray(), (resultSet, row) -> {
            var user =  new UserRow();
            user.setId(UUID.fromString(resultSet.getString("id")));
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setEmail(resultSet.getString("email"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            user.setEmailVerified(resultSet.getBoolean("email_verified"));
            user.setLocked(resultSet.getBoolean("is_locked"));
            user.setBadLoginAtemptsInRow(resultSet.getInt("bad_login_atempts_in_row"));
            user.setCaloriesPerDay(resultSet.getBigDecimal("calories_per_day"));

            return user;
        }).toArray(new UserRow[]{});

        Long count = null;
        if (shouldCountAllRows)
        {
            var countQuery = queryBuilder.toCountQueryBuilder().toSqlFragment();
            count = jdbc.queryForObject(countQuery.getSql(), countQuery.getBinds().toArray(), Long.class);
        }

        return new UserListResponse(
                Arrays.copyOfRange(result, 0, Math.min(result.length, size)),
                checkNext ? result.length == size + 1 : null,
                count
        );
    }


    @Transactional
    public void saveAfterInvitation(UserLogin userLogin, InvitationToken invitationToken)
    {
        entityManager.persist(userLogin);
        entityManager.remove(invitationToken);
        entityManager.flush();

    }

    @Transactional
    public RefreshToken saveAfterLogin(UserLogin userLogin)
    {
        userLogin.resetUnSuccesfullLoginAtemptsInRow();
        var refreshToken = new RefreshToken();
        refreshToken.setUserLogin(userLogin);

        entityManager.persist(userLogin);
        entityManager.persist(refreshToken);
        entityManager.flush();

        return refreshToken;
    }

    @Transactional
    public void save(UserLogin userLogin)
    {
        entityManager.persist(userLogin);
        entityManager.flush();
    }

    public void delete(UserLogin userLogin)
    {
        users.delete(userLogin);
        users.flush();
    }

    public UserLogin getById(UUID userId) {
        return users.findById(userId).orElseThrow(MissingEntityException::new);
    }

    public Optional<UserLogin> findByEmail(String email) {
        return users.findByEmail(email);
    }

    @Transactional
    public void saveAfterNewPasswordWasSet(UserLogin user) {
        entityManager.persist(user);
        entityManager.remove(user.getLostPasswordToken());
        entityManager.flush();
    }
}
