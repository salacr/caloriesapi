package org.salac.toptal.task2.dto.repository;

import org.salac.toptal.task2.dto.InvitationToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface InvitationTokenRepository extends JpaRepository<InvitationToken, UUID> {

    Optional<InvitationToken> findByEmail(String email);

    InvitationToken getByEmail(String email);
}
