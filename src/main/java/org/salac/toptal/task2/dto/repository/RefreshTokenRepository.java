package org.salac.toptal.task2.dto.repository;

import org.salac.toptal.task2.dto.RefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, UUID> {

}
