package org.salac.toptal.task2.dto.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;
import java.util.UUID;

public class MealRow {

    private UUID id = UUID.randomUUID();

    private UUID user;

    private LocalDate date;

    private LocalTime time;

    private String text;

    private BigDecimal calories;

    private BigDecimal consumedCaloriesPerDay;

    private boolean isLessThenExpected;

    public MealRow() {
    }

    public MealRow(UUID id, BigDecimal calories, LocalDate date, boolean isLessThenExpected, String text, LocalTime time, UUID user, BigDecimal consumedCaloriesPerDay) {
        this.id = id;
        this.user = user;
        this.date = date;
        this.time = time;
        this.text = text;
        this.calories = calories;
        this.isLessThenExpected = isLessThenExpected;
        this.consumedCaloriesPerDay = consumedCaloriesPerDay;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUser() {
        return user;
    }

    public void setUser(UUID user) {
        this.user = user;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public BigDecimal getCalories() {
        return calories;
    }

    public BigDecimal getConsumedCaloriesPerDay() {
        return consumedCaloriesPerDay;
    }

    public void setConsumedCaloriesPerDay(BigDecimal consumedCaloriesPerDay) {
        this.consumedCaloriesPerDay = consumedCaloriesPerDay;
    }

    public void setCalories(BigDecimal calories) {
        this.calories = calories;
    }

    public boolean isLessThenExpected() {
        return isLessThenExpected;
    }



    public void setLessThenExpected(boolean lessThenExpected) {
        isLessThenExpected = lessThenExpected;
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MealRow mealRow = (MealRow) o;
        return isLessThenExpected == mealRow.isLessThenExpected &&
                Objects.equals(id, mealRow.id) &&
                Objects.equals(user, mealRow.user) &&
                Objects.equals(date, mealRow.date) &&
                Objects.equals(time, mealRow.time) &&
                Objects.equals(text, mealRow.text) &&
                Objects.equals(calories, mealRow.calories) &&
                Objects.equals(consumedCaloriesPerDay, mealRow.consumedCaloriesPerDay);
    }

    @Override
    public String toString() {
        return "MealRow{" +
                "id=" + id +
                ", user=" + user +
                ", date=" + date +
                ", time=" + time +
                ", text='" + text + '\'' +
                ", calories=" + calories +
                ", consumedCaloriesPerDay=" + consumedCaloriesPerDay +
                ", isLessThenExpected=" + isLessThenExpected +
                '}';
    }
}
