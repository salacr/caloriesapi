package org.salac.toptal.task2.dto;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

@Entity
public class UserLogin {

    private static final int MAX_UNSUCCESFULL_LOGIN_ATEMPTS_IN_A_ROW = 3;

    @Id
    @Column
    private final UUID id = UUID.randomUUID();

    @Column(unique = true)
    private String email;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    @Column
    private Role role = Role.ROLE_USER;

    @Column
    private boolean isLocked = false;

    @Column
    private boolean emailVerified = false;

    @Column
    private int badLoginAtemptsInRow = 0;

    @Column
    private BigDecimal caloriesPerDay;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "userLogin")
    private EmailVerificationToken emailVerificationToken;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "userLogin")
    @OrderBy("date DESC, time DESC")
    private List<Meal> meals = new LinkedList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "userLogin")
    private List<RefreshToken> refreshTokens = new LinkedList<>();

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "userLogin")
    private LostPasswordToken lostPasswordToken;

    public UserLogin() {
    }

    public UserLogin(String email, String firstName, String lastName, String password) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
        this.emailVerified = false;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void lock() {
        isLocked = true;
    }

    public void unlock() {
        badLoginAtemptsInRow = 0;
        isLocked = false;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified() {
        this.emailVerified = true;
        this.emailVerificationToken = null;
    }

    public void setEmailVerificationToken(EmailVerificationToken emailVerificationToken) {
        this.emailVerificationToken = emailVerificationToken;
        emailVerificationToken.setUserLogin(this);
    }

    public BigDecimal getCaloriesPerDay() {
        return caloriesPerDay;
    }

    public void setCaloriesPerDay(BigDecimal caloriesPerDay) {
        this.caloriesPerDay = caloriesPerDay;
    }

    public int getBadLoginAtemptsInRow() {
        return badLoginAtemptsInRow;
    }

    public void resetUnSuccesfullLoginAtemptsInRow() {
        this.badLoginAtemptsInRow = 0;
    }

    public void logUnSuccesfullLoginAtemptsInRow() {
        this.badLoginAtemptsInRow++;
        if (badLoginAtemptsInRow >= MAX_UNSUCCESFULL_LOGIN_ATEMPTS_IN_A_ROW)
        {
            lock();
        }
    }

    public EmailVerificationToken getEmailVerificationToken() {
        return emailVerificationToken;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public LostPasswordToken getLostPasswordToken() {
        return lostPasswordToken;
    }

    public void setLostPasswordToken(LostPasswordToken lostPasswordToken) {
        this.lostPasswordToken = lostPasswordToken;
    }
}
