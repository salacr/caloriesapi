package org.salac.toptal.task2.dto.repository;

import org.salac.toptal.task2.dto.LostPasswordToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LostPasswordTokenRepository extends JpaRepository<LostPasswordToken, UUID> {

}
