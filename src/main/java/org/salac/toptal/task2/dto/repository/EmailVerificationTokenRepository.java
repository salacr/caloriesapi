package org.salac.toptal.task2.dto.repository;

import org.salac.toptal.task2.dto.EmailVerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface EmailVerificationTokenRepository extends JpaRepository<EmailVerificationToken, UUID> {
}
