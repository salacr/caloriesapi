package org.salac.toptal.task2.dto.service;

public class MealListResponse {

    private MealRow[] data;

    private Boolean next;

    private Long count;

    public MealListResponse() {
    }

    public MealListResponse(MealRow[] data, Boolean hasNext, Long count) {
        this.data = data;
        this.next = hasNext;
        this.count = count;
    }

    public MealRow[] getData() {
        return data;
    }

    public Boolean isNext() {
        return next;
    }

    public Long getCount() {
        return count;
    }
}
