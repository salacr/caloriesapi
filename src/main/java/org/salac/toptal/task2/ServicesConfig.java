package org.salac.toptal.task2;

import org.salac.toptal.task2.service.CaloriesCalculator;
import org.salac.toptal.task2.service.NutritionixClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServicesConfig {

    @Bean
    public CaloriesCalculator getCaloriesCalculator(@Value("${nutritionix.key}") String key, @Value("${nutritionix.appId}") String appId)
    {
        return new NutritionixClient(appId, key);
    }

}
