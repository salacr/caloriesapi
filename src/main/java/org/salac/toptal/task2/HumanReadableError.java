package org.salac.toptal.task2;

import java.util.Objects;

public class HumanReadableError {

    private String defaultMessage;

    /**
     * Used by jackson
     */
    public HumanReadableError() {
    }

    public HumanReadableError(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }


    public String getDefaultMessage() {
        return defaultMessage;
    }

    @Override
    public String toString() {
        return "HumanReadableError{" +
                "defaultMessage='" + defaultMessage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HumanReadableError that = (HumanReadableError) o;
        return Objects.equals(defaultMessage, that.defaultMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(defaultMessage);
    }
}
