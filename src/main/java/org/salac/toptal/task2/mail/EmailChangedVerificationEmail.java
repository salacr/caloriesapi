package org.salac.toptal.task2.mail;

import org.salac.toptal.task2.dto.UserLogin;
import org.springframework.mail.SimpleMailMessage;

public class EmailChangedVerificationEmail extends SimpleMailMessage {

    public EmailChangedVerificationEmail(UserLogin userLogin, String serverName)
    {
        super();
        setTo(userLogin.getEmail());
        setSubject("Toptal-Task e-mail was changed");
        setText("Your e-mail was changed, please click on this link to verify your new e-mail address: " + serverName + "/auth/verify-email/" + userLogin.getEmailVerificationToken().getId());
    }

}
