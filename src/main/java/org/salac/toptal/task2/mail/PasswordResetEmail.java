package org.salac.toptal.task2.mail;

import org.salac.toptal.task2.dto.LostPasswordToken;
import org.springframework.mail.SimpleMailMessage;

public class PasswordResetEmail extends SimpleMailMessage {

    public PasswordResetEmail(LostPasswordToken token, String serverName)
    {
        super();
        setTo(token.getUserLogin().getEmail());
        setSubject("Toptal-Task password reset");
        setText("You can reset your password on folowing link: " + serverName + "/auth/new-password/" + token.getId());
    }

}

