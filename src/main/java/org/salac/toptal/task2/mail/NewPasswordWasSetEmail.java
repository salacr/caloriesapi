package org.salac.toptal.task2.mail;

import org.salac.toptal.task2.dto.UserLogin;
import org.springframework.mail.SimpleMailMessage;

public class NewPasswordWasSetEmail extends SimpleMailMessage {

    public NewPasswordWasSetEmail(UserLogin user)
    {
        super();
        setTo(user.getEmail());
        setSubject("Toptal-Task password changed");
        setText("You password was changed, if you don't do that. Please contact your administrator.");
    }

}

