package org.salac.toptal.task2.mail;

import org.springframework.mail.SimpleMailMessage;

public class EmailChangedNotificationEmail extends SimpleMailMessage {

    public EmailChangedNotificationEmail(String oldEmail, String newEmail)
    {
        super();
        setTo(oldEmail);
        setSubject("Toptal-Task e-mail was changed");
        setText("Your e-mail was changed, to:" + newEmail + " if you disagree with this change please contact administrator");
    }

}
