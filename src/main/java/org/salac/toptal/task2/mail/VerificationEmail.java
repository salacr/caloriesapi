package org.salac.toptal.task2.mail;

import org.salac.toptal.task2.dto.UserLogin;
import org.springframework.mail.SimpleMailMessage;

public class VerificationEmail extends SimpleMailMessage {

    public VerificationEmail(UserLogin userLogin, String serverName)
    {
        super();
        setTo(userLogin.getEmail());
        setSubject("Toptal-Task e-mail verification");
        setText("You are registered, please click on this link: " + serverName + "/auth/verify-email/" + userLogin.getEmailVerificationToken().getId());
    }

}
