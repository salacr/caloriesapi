package org.salac.toptal.task2.mail;

import org.salac.toptal.task2.dto.InvitationToken;
import org.springframework.mail.SimpleMailMessage;

public class InvitationEmail extends SimpleMailMessage {

    public InvitationEmail(InvitationToken invitationToken, String serverName)
    {
        super();
        setTo(invitationToken.getEmail());
        setSubject("Toptal-Task invitation");
        setText("You are invited to our greate app! Please register on the folowing link: " + serverName + "/auth/from-invitation/" + invitationToken.getId());
    }

}
