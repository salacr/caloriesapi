package org.salac.toptal.task2.controller;

import org.salac.toptal.task2.ValidationFailure;
import org.salac.toptal.task2.auth.JWTAuthenticationToken;
import org.salac.toptal.task2.controller.user.ChangeRoleRequest;
import org.salac.toptal.task2.dto.EmailVerificationToken;
import org.salac.toptal.task2.dto.Role;
import org.salac.toptal.task2.dto.service.UserListResponse;
import org.salac.toptal.task2.dto.service.UserService;
import org.salac.toptal.task2.exception.ForbiddenException;
import org.salac.toptal.task2.exception.InvalidDataException;
import org.salac.toptal.task2.controller.user.InviteRequest;
import org.salac.toptal.task2.controller.user.UpdateUserRequest;
import org.salac.toptal.task2.dto.InvitationToken;
import org.salac.toptal.task2.dto.UserLogin;
import org.salac.toptal.task2.dto.repository.InvitationTokenRepository;
import org.salac.toptal.task2.exception.UserException;
import org.salac.toptal.task2.mail.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Controller responsible for handling user requests
 */
@RestController
@RequestMapping("/user")
public class User {

    private final UserService users;

    private final InvitationTokenRepository invitations;

    private final MailSender mailSender;

    private final PasswordEncoder passwordEncoder;

    private final String serverName;

    public User(@Autowired PasswordEncoder passwordEncoder,
                @Value("${app.servername}") String serverName,
                @Autowired MailSender mailSender,
                @Autowired UserService users,
                @Autowired InvitationTokenRepository invitations) {
        this.users = users;
        this.invitations = invitations;
        this.mailSender = mailSender;
        this.passwordEncoder = passwordEncoder;
        this.serverName = serverName;
    }

    // list

    @GetMapping(value = "")
    public ResponseEntity<UserListResponse> list(
            JWTAuthenticationToken authentication,
            Pageable pageable,
            @RequestParam("calcCount") Optional<Boolean> calcCount,
            @RequestParam("checkNext") Optional<Boolean> checkNext,
            @RequestParam("filter") Optional<String> filter
    ) {
        return ResponseEntity.ok(
                users.list(
                        authentication.getUser(),
                        pageable,
                        checkNext.orElse(true),
                        calcCount.orElse(false),
                        filter.orElse("")
                )
        );
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@AuthenticationPrincipal UserLogin currentUser, @PathVariable("id") UUID id, @Valid @RequestBody UpdateUserRequest request, BindingResult bindingResult) {
        var user = users.getById(id);
        if (currentUser.getRole() == Role.ROLE_USER && !currentUser.getId().equals(id))
        {
            throw new ForbiddenException();
        }
        InvalidDataException.throwIfHasError(bindingResult);



        if (request.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(request.getPassword()));
            mailSender.send(new NewPasswordWasSetEmail(user));
        }
        user.setLastName(request.getLastName());
        user.setFirstName(request.getFirstName());
        user.setCaloriesPerDay(request.getCaloriesPerDay());
        if (!user.getEmail().equals(request.getEmail()))
        {
            if (users.findByEmail(request.getEmail()).isPresent())
            {
                throw new InvalidDataException(
                        new ValidationFailure(
                                Set.of(new FieldError("UpdateUserRequest", "email", "UserLogin with this e-mail already exists."))
                        )
                );
            }

            user.setEmailVerificationToken(new EmailVerificationToken());
            mailSender.send(new EmailChangedNotificationEmail(user.getEmail(), request.getEmail()));
            user.setEmail(request.getEmail());
            mailSender.send(new EmailChangedVerificationEmail(user, serverName));
        }

        users.save(user);
        return ResponseEntity.noContent().build();
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@AuthenticationPrincipal UserLogin currentUser, @PathVariable("id") UUID id) {
        var user = users.getById(id);
        if (currentUser.getRole() == Role.ROLE_ADMIN)
        {
            if (id.equals(currentUser.getId()))
            {
                throw new UserException("Admin can't delete itself");
            }
        }
        else if (currentUser.getRole() == Role.ROLE_USER)
        {
            if (!id.equals(currentUser.getId()))
            {
                throw new ForbiddenException();
            }
        }

        users.delete(user);
        return ResponseEntity.noContent().build();
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @PostMapping(value = "/change-role/{userId}")
    public ResponseEntity<?> promote(@AuthenticationPrincipal UserLogin currentUser, @PathVariable("userId") UUID userId, @Valid @RequestBody ChangeRoleRequest request, BindingResult bindingResult) {
        var user = users.getById(userId);
        InvalidDataException.throwIfHasError(bindingResult);
        if (user.getRole() == request.getRole())
        {
            throw new UserException("This role is already set");
        }
        if (currentUser.getId().equals(userId))
        {
            throw new UserException("You can't change role on your own account");
        }
        user.setRole(request.getRole());
        users.save(user);
        return ResponseEntity.noContent().build();
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @PostMapping(value = "/unlock/{id}")
    public ResponseEntity<?> unlock(@PathVariable("id") UUID id) {
        var user = users.getById(id);
        if (!user.isLocked())
        {
            throw new UserException("User isn't locked");
        }
        user.unlock();
        users.save(user);
        return ResponseEntity.noContent().build();
    }

    @Secured({"ROLE_ADMIN"})
    @PostMapping(value = "/invite")
    public ResponseEntity<?> create(@Value("${app.servername:http://localhost:8888}") String serverName, @Valid @RequestBody InviteRequest request, BindingResult bindingResult) {
        InvalidDataException.throwIfHasError(bindingResult);

        var invitation = new InvitationToken();
        invitation.setEmail(request.getEmail());
        invitations.saveAndFlush(invitation);

        mailSender.send(new InvitationEmail(invitation, serverName));

        return ResponseEntity.noContent().build();
    }
}
