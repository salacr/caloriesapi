package org.salac.toptal.task2.controller;

import org.salac.toptal.task2.controller.auth.*;
import org.salac.toptal.task2.dto.*;
import org.salac.toptal.task2.dto.repository.*;
import org.salac.toptal.task2.dto.service.LostPasswordService;
import org.salac.toptal.task2.dto.service.UserService;
import org.salac.toptal.task2.exception.InvalidDataException;
import org.salac.toptal.task2.exception.MissingEntityException;
import org.salac.toptal.task2.auth.TokenAuthenticationService;
import org.salac.toptal.task2.mail.NewPasswordWasSetEmail;
import org.salac.toptal.task2.mail.PasswordResetEmail;
import org.salac.toptal.task2.mail.VerificationEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;


/**
 * Controller responsible for handling auth requests
 */
@RestController
@RequestMapping("/auth")
public class Auth {

    private final UserService users;

    private final EmailVerificationTokenRepository verificationTokens;

    private final InvitationTokenRepository invitationTokens;

    private final RefreshTokenRepository refreshTokenRepository;

    private final PasswordEncoder passwordEncoder;

    private final MailSender mailSender;

    private final String serverName;

    private final LostPasswordService lostPasswordService;

    private final TokenAuthenticationService tokenAuthenticationService;

    public Auth(@Autowired TokenAuthenticationService tokenAuthenticationService,
                @Autowired MailSender mailSender,
                @Autowired InvitationTokenRepository invitationTokens,
                @Autowired RefreshTokenRepository refreshTokenRepository,
                @Autowired EmailVerificationTokenRepository verificationTokens,
                @Value("${app.servername}") String serverName,
                @Autowired UserService users,
                @Autowired LostPasswordService lostPasswordService,
                @Autowired PasswordEncoder passwordEncoder) {
        this.users = users;
        this.verificationTokens = verificationTokens;
        this.invitationTokens = invitationTokens;
        this.passwordEncoder = passwordEncoder;
        this.mailSender = mailSender;
        this.serverName = serverName;
        this.refreshTokenRepository = refreshTokenRepository;
        this.lostPasswordService = lostPasswordService;
        this.tokenAuthenticationService = tokenAuthenticationService;
    }



    @PostMapping(value = "/sign-in")
    public ResponseEntity<UUID> signIn(@Valid @RequestBody SignInRequest request, BindingResult bindingResult) {
        InvalidDataException.throwIfHasError(bindingResult);


        var user = new UserLogin();
        user.setEmail(request.getEmail());
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setEmailVerificationToken(new EmailVerificationToken());
        user.setCaloriesPerDay(request.getCaloriesPerDay());
        users.save(user);

        Optional<InvitationToken> invitationToken = invitationTokens.findByEmail(user.getEmail());
        if (invitationToken.isPresent()) {
            invitationTokens.delete(invitationToken.get());
            invitationTokens.flush();
        }

        mailSender.send(new VerificationEmail(user, serverName));

        return ResponseEntity.status(HttpStatus.CREATED).body(user.getId());
    }

    @GetMapping(value = "/verify-email/{requestToken}")
    public ResponseEntity<?> verifyEmail(@PathVariable("requestToken") UUID requestToken) {
        var verificationToken = verificationTokens.findById(requestToken).orElseThrow(MissingEntityException::new);

        var user = verificationToken.getUserLogin();
        user.setEmailVerified();

        verificationTokens.delete(verificationToken);
        verificationTokens.flush();
        users.save(user);

        return ResponseEntity.noContent().build();
    }

    /**
     * This is also used when user "forgets to verify his email"
     */
    @PostMapping(value = "/lost-password")
    public ResponseEntity<?> lostPassword(@Valid @RequestBody LostPasswordRequest request, BindingResult bindingResult) {
        InvalidDataException.throwIfHasError(bindingResult);
        var user = users.findByEmail(request.getEmail()).orElseThrow(MissingEntityException::new);


        var lostPasswordToken = lostPasswordService.getNewLostPasswordToken(user);
        mailSender.send(new PasswordResetEmail(lostPasswordToken, serverName));

        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/new-password/{lostPasswordRequestToken}")
    public ResponseEntity<?> newPassword(@PathVariable("lostPasswordRequestToken") UUID lostPasswordRequestToken, @Valid @RequestBody NewPasswordRequest request, BindingResult bindingResult) {
        InvalidDataException.throwIfHasError(bindingResult);
        var lostPasswordToken = lostPasswordService.getById(lostPasswordRequestToken);

        var user = lostPasswordToken.getUserLogin();

        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setEmailVerified();
        users.saveAfterNewPasswordWasSet(user);

        mailSender.send(new NewPasswordWasSetEmail(user));


        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/from-invitation/{requestToken}")
    public ResponseEntity<?> fromInvitation(@PathVariable("requestToken") UUID requestToken, @Valid @RequestBody InvitationRequest request, BindingResult bindingResult) {
        InvalidDataException.throwIfHasError(bindingResult);

        var invitationToken = invitationTokens.findById(requestToken).orElseThrow(MissingEntityException::new);

        var user = new UserLogin();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEmail(invitationToken.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setCaloriesPerDay(request.getCaloriesPerDay());
        user.setEmailVerified();

        users.saveAfterInvitation(user, invitationToken);

        return ResponseEntity.status(HttpStatus.CREATED).body(user.getId());
    }

    @PostMapping(value = "/login")
    public ResponseEntity<LogInResponse> login(@Valid @RequestBody LogInRequest loginRequest, BindingResult bindingResult) {
        InvalidDataException.throwIfHasError(bindingResult);

        var user = users.findByEmail(loginRequest.getEmail()).orElseThrow(MissingEntityException::new);
        if (!user.isEmailVerified())
        {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new LogInResponse(null, "E-mail isn't verified"));
        }
        if (user.isLocked())
        {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new LogInResponse(null, "Account is locked please contact administrator or manager"));
        }
        if (!passwordEncoder.matches(loginRequest.getPassword(), user.getPassword()))
        {
            user.logUnSuccesfullLoginAtemptsInRow();
            users.save(user);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LogInResponse(null, "Wrong email or password"));
        }

        var jwtToken = tokenAuthenticationService.createJwtToken(user.getId());
        var refreshToken = users.saveAfterLogin(user);

        return ResponseEntity.ok(new LogInResponse(jwtToken, refreshToken.getId()));
    }


    @PostMapping(value = "/logout/{requestToken}")
    public ResponseEntity<?> logout(@PathVariable("requestToken") UUID requestToken) {
        var refreshToken = refreshTokenRepository.findById(requestToken).orElseThrow(MissingEntityException::new);

        refreshTokenRepository.delete(refreshToken);
        refreshTokenRepository.flush();

        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/refresh/{requestToken}")
    public ResponseEntity<RefreshResponse> refresh(@PathVariable("requestToken") UUID requestToken) {
        var refreshToken = refreshTokenRepository.findById(requestToken).orElseThrow(MissingEntityException::new);
        var jwtToken = tokenAuthenticationService.createJwtToken(refreshToken.getUserLogin().getId());
        return ResponseEntity.ok(new RefreshResponse(jwtToken));

    }


}
