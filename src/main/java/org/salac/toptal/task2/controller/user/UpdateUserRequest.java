package org.salac.toptal.task2.controller.user;

import org.salac.toptal.task2.constraint.PasswordStrength;
import org.springframework.lang.NonNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * POJO class which holds data from request together with validation
 */
public class UpdateUserRequest {

    @NotNull
    @Size(min = 1, max = 255)
    @Email
    private String email;

    @NotNull
    @Size(min = 1, max = 255)
    private String firstName;

    @NotNull
    @Size(min = 1, max = 255)
    private String lastName;

    // during the update the password isn't required so don't enforce the change
    @PasswordStrength(minLength = 8)
    private String password;

    @Min(0)
    @NonNull
    private BigDecimal caloriesPerDay;

    public UpdateUserRequest() {
    }

    public UpdateUserRequest(String email, String firstName, String lastName, String password, BigDecimal caloriesPerDay) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.caloriesPerDay = caloriesPerDay;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public BigDecimal getCaloriesPerDay() {
        return caloriesPerDay;
    }

    public String getEmail() {
        return email;
    }
}
