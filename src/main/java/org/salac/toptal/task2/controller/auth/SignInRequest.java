package org.salac.toptal.task2.controller.auth;

import org.salac.toptal.task2.constraint.PasswordStrength;
import org.salac.toptal.task2.constraint.UniqueUserEmail;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * POJO class which holds data from request together with validation
 */
public class SignInRequest {

    @NotNull
    @Size(min = 1, max = 255)
    private String firstName;

    @NotNull
    @Size(min = 1, max = 255)
    private String lastName;

    @NotNull
    @Size(min = 1, max = 255)
    @Email
    @UniqueUserEmail
    private String email;

    @NotNull
    @PasswordStrength(minLength = 8)
    private String password;

    @Min(0)
    @NotNull
    private BigDecimal caloriesPerDay;

    public SignInRequest() {
    }

    public SignInRequest(String firstName, String lastName, String email, String password, BigDecimal caloriesPerDay) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.caloriesPerDay =caloriesPerDay;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public BigDecimal getCaloriesPerDay() {
        return caloriesPerDay;
    }
}
