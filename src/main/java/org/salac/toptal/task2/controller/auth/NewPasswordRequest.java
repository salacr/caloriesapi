package org.salac.toptal.task2.controller.auth;

import org.salac.toptal.task2.constraint.PasswordStrength;

import javax.validation.constraints.NotNull;

/**
 * POJO class which holds data from request together with validation
 */
public class NewPasswordRequest {

    @NotNull
    @PasswordStrength(minLength = 8)
    private String password;

    public NewPasswordRequest() {
    }

    public NewPasswordRequest(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
