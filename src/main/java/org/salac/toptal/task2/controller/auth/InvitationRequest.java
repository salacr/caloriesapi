package org.salac.toptal.task2.controller.auth;

import org.salac.toptal.task2.constraint.PasswordStrength;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * POJO class which holds data from request together with validation
 */
public class InvitationRequest {

    @NotNull
    @Size(min = 1, max = 255)
    private String firstName;

    @NotNull
    @Size(min = 1, max = 255)
    private String lastName;

    @NotNull
    @PasswordStrength(minLength = 8)
    private String password;

    @Min(0)
    @NotNull
    private BigDecimal caloriesPerDay;

    public InvitationRequest() {
    }

    public InvitationRequest(String firstName, String lastName, String password, BigDecimal caloriesPerDay) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.caloriesPerDay = caloriesPerDay;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public BigDecimal getCaloriesPerDay() {
        return caloriesPerDay;
    }
}
