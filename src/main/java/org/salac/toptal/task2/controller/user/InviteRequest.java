package org.salac.toptal.task2.controller.user;

import org.salac.toptal.task2.constraint.UniqueUserEmail;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * POJO class which holds data from request together with validation
 */
public class InviteRequest {

    @NotNull
    @Size(min = 1, max = 255)
    @Email
    @UniqueUserEmail
    private String email;

    public InviteRequest() {
    }

    public InviteRequest(@NotNull @Size(min = 1, max = 255) @Email String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
