package org.salac.toptal.task2.controller.meal;

import org.salac.toptal.task2.constraint.CreateMealUserId;
import org.salac.toptal.task2.constraint.MealIsntInFuture;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import java.util.UUID;

/**
 * POJO class which holds data from request together with validation
 */
@MealIsntInFuture
public class MealRequest {

    @CreateMealUserId
    private UUID userId;

    @NotNull
    private LocalDate date;

    @NotNull
    private LocalTime time;

    @NotNull
    @Size(min = 1, max = 255)
    private String text;

    @Min(0)
    private BigDecimal calories;

    public MealRequest() {
    }

    public MealRequest(UUID userId, LocalDate date, LocalTime time, String text, BigDecimal calories) {
        this.userId = userId;
        this.date = date;
        this.time = time;
        this.text = text;
        this.calories = calories;
    }

    public LocalDate getDate() {
        return date;
    }

    public LocalTime getTime() {
        return time;
    }

    public String getText() {
        return text;
    }

    public Optional<BigDecimal> getCalories() {
        return Optional.ofNullable(calories);
    }

    public Optional<UUID> getUserId() {
        return Optional.ofNullable(userId);
    }
}
