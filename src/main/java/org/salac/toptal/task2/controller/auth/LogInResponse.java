package org.salac.toptal.task2.controller.auth;

import java.util.UUID;

/**
 * Data used as a response
 */
public class LogInResponse {

    private String message;

    private String jwt;

    private UUID refreshToken;

    /**
     * Used by jackson
     */
    public LogInResponse() {

    }

    public LogInResponse(String jwtToken) {
        this.jwt = jwtToken;
    }

    public LogInResponse(String jwtToken, String message) {
        this.jwt = jwtToken;
        this.message = message;
    }

    public LogInResponse(String jwt, UUID refreshToken) {
        this.jwt = jwt;
        this.refreshToken = refreshToken;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UUID getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(UUID refreshToken) {
        this.refreshToken = refreshToken;
    }
}
