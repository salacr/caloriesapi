package org.salac.toptal.task2.controller.auth;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * POJO class which holds data from request together with validation
 */
public class LostPasswordRequest {

    @NotNull
    @Size(min = 1, max = 255)
    @Email
    private String email;

    public LostPasswordRequest() {
    }

    public LostPasswordRequest(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
