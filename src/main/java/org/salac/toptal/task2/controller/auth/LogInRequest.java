package org.salac.toptal.task2.controller.auth;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * POJO class which holds data from request together with validation
 */
public class LogInRequest {

    @NotNull
    @Size(min = 1, max = 255)
    @Email
    private String email;

    @NotNull
    private String password;

    public LogInRequest() {
    }

    public LogInRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
