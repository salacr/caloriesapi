package org.salac.toptal.task2.controller.auth;


/**
 * Data used as a response
 */
public class RefreshResponse {


    private String jwt;

    /**
     * Used by jackson
     */
    public RefreshResponse() {

    }

    public RefreshResponse(String jwtToken) {
        this.jwt = jwtToken;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
