package org.salac.toptal.task2.controller;

import org.salac.toptal.task2.dto.repository.UserLoginRepository;
import org.salac.toptal.task2.dto.service.MealListResponse;
import org.salac.toptal.task2.dto.service.MealService;
import org.salac.toptal.task2.exception.InvalidDataException;
import org.salac.toptal.task2.auth.JWTAuthenticationToken;
import org.salac.toptal.task2.controller.meal.MealRequest;
import org.salac.toptal.task2.service.CaloriesCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

/**
 * Controller responsible for handling meal requests
 */
@RestController
@RequestMapping("/meal")
public class Meal {

    private final CaloriesCalculator caloriesCalculator;

    private final MealService meals;

    private final UserLoginRepository userLogins;

    public Meal(@Autowired UserLoginRepository userLogins, @Autowired MealService meals, @Autowired CaloriesCalculator caloriesCalculator) {
        this.meals = meals;
        this.caloriesCalculator = caloriesCalculator;
        this.userLogins = userLogins;
    }

    @GetMapping(value = "")
    public ResponseEntity<MealListResponse> list(
            JWTAuthenticationToken authentication,
            Pageable pageable,
            @RequestParam("calcCount") Optional<Boolean> calcCount,
            @RequestParam("checkNext") Optional<Boolean> checkNext,
            @RequestParam("filter") Optional<String> filter) {
        return ResponseEntity.ok(
                meals.list(
                        authentication.getUser(),
                        pageable,
                        checkNext.orElse(true),
                        calcCount.orElse(false),
                        filter.orElse("")
                )
        );
    }

    @PostMapping(value = "")
    public ResponseEntity<UUID> create(@Valid @RequestBody MealRequest request, BindingResult bindingResult, JWTAuthenticationToken authentication) {
        InvalidDataException.throwIfHasError(bindingResult);

        var meal = new org.salac.toptal.task2.dto.Meal();
        meal.setText(request.getText());
        meal.setDate(request.getDate());
        meal.setTime(request.getTime());
        meal.setUserLogin(request.getUserId().map(userLogins::getOne).orElse(authentication.getUser()));
        meal.setCalories(
           request.getCalories().orElseGet(() -> caloriesCalculator.getCalories(request.getText()))
        );

        meals.save(meal);

        return ResponseEntity.status(HttpStatus.CREATED).body(meal.getId());
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable("id") UUID id, @Valid @RequestBody MealRequest request, BindingResult bindingResult, JWTAuthenticationToken authentication) {
        var meal = meals.getByUserLoginAndId(authentication.getUser(), id);
        InvalidDataException.throwIfHasError(bindingResult);

        meal.setText(request.getText());
        meal.setDate(request.getDate());
        meal.setTime(request.getTime());
        meal.setUserLogin(request.getUserId().map(userLogins::getOne).orElse(authentication.getUser()));
        meal.setCalories(
                request.getCalories().orElseGet(() -> caloriesCalculator.getCalories(request.getText()))
        );

        meals.save(meal);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") UUID id, JWTAuthenticationToken authentication) {
        var meal = meals.getByUserLoginAndId(authentication.getUser(), id);
        meals.delete(meal);
        return ResponseEntity.noContent().build();
    }

}
