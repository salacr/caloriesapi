package org.salac.toptal.task2.controller.user;

import org.salac.toptal.task2.constraint.AvailableRoleToSet;
import org.salac.toptal.task2.dto.Role;

/**
 * POJO class which holds data from request together with validation
 */
public class ChangeRoleRequest {

    @AvailableRoleToSet
    private Role role;

    public ChangeRoleRequest() {
    }

    public ChangeRoleRequest(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
