package org.salac.toptal.task2.cron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class CleanOldTokens {

    private static final Logger log = LoggerFactory.getLogger(CleanOldTokens.class);

    private final JdbcTemplate jdbc;

    public CleanOldTokens(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Scheduled(fixedRate = 3600_000)
    public void cleanOldTokens() {
        log.info("Cleaning old tokens");
        // for security reason we don't want this token to be valid for long time
        jdbc.execute("DELETE FROM lost_password_token WHERE created < (NOW() - INTERVAL '1 DAY')");
        // users are able to verify their email also by "reset password" functionality
        jdbc.execute("DELETE FROM email_verification_token WHERE created < (NOW() - INTERVAL '30 DAY')");
        jdbc.execute("DELETE FROM invitation_token WHERE created < (NOW() - INTERVAL '30 DAY')");
    }
}
