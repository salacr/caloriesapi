package org.salac.toptal.task2.exception;

public class UserException extends RuntimeException{
    public UserException(String message) {
        super(message);
    }
}
