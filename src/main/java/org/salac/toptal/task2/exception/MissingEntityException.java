package org.salac.toptal.task2.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is thrown when requested entity is not found
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class MissingEntityException extends RuntimeException{

}
