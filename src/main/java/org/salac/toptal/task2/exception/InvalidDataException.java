package org.salac.toptal.task2.exception;

import org.salac.toptal.task2.ValidationFailure;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is thrown when user submit request which fails validation
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidDataException extends RuntimeException{

    private final ValidationFailure failures;

    public InvalidDataException(ValidationFailure failure) {
        super("Error:");
        this.failures = failure;
    }

    public InvalidDataException(BindingResult bindingResult) {
        super("Error:" + bindingResult);
        this.failures = new ValidationFailure(bindingResult.getGlobalErrors(), bindingResult.getFieldErrors());
    }

    public ValidationFailure getFailures() {
        return failures;
    }

    public static void throwIfHasError(BindingResult result)
    {
        if (result.hasErrors())
        {
            throw new InvalidDataException(result);
        }
    }
}
