package org.salac.toptal.task2;

import org.salac.toptal.task2.filterParser.SQLFragment;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleQueryBuilder {

    private final StringBuilder selectPart = new StringBuilder();
    private final List<SQLFragment> whereParts = new LinkedList<>();
    private final StringBuilder orderPart = new StringBuilder();
    private final StringBuilder fromPart = new StringBuilder();

    private final List<Object> binds = new LinkedList<>();

    private Integer limit;

    private Integer offset;

    private SimpleQueryBuilder() {
    }

    public static SimpleQueryBuilder select(String... columns)
    {
        SimpleQueryBuilder qb = new SimpleQueryBuilder();
        for(var column : columns)
        {
            qb.selectPart.append(column).append("\n");
        }
        return qb;
    }

    public SimpleQueryBuilder from(String table)
    {
        fromPart.append(table).append("\n");
        return this;
    }

    public SimpleQueryBuilder andWhere(String where)
    {
        whereParts.add(new SQLFragment("(" + where + ")\n"));
        return this;
    }

    public SimpleQueryBuilder andWhere(String where, Object... binds)
    {
        andWhere(where);
        this.binds.addAll(Arrays.asList(binds));
        return this;
    }

    public SimpleQueryBuilder addOrderBy(String column, boolean asc)
    {
        orderPart.append(column).append(" ").append(asc ? "ASC" : "DESC").append("\n");
        return this;
    }

    public SimpleQueryBuilder setLimit(Integer limit) {
        this.limit = limit;
        return this;

    }

    public SimpleQueryBuilder setOffset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public String getSql() {

        StringBuilder completeQuery = new StringBuilder();
        completeQuery.append("SELECT \n")
                .append(selectPart.toString())
                .append("FROM \n")
                .append(fromPart.toString());
        if (whereParts.size() > 0)
        {
            String whereFragment = whereParts.stream().map(SQLFragment::getSql).collect(Collectors.joining(" AND "));
            completeQuery.append("WHERE \n")
                    .append(whereFragment);
        }
        if (orderPart.length() > 0)
        {
            completeQuery.append("ORDER BY \n")
                    .append(orderPart.toString());
        }
        if (limit != null)
        {
            completeQuery.append("LIMIT ").append(limit).append("\n");
        }
        if (offset != null)
        {
            completeQuery.append("OFFSET ").append(offset).append("\n");
        }
        return completeQuery.toString();
    }

    public SQLFragment toSqlFragment()
    {
        return new SQLFragment(this.getSql(), binds);
    }

    public SimpleQueryBuilder toCountQueryBuilder()
    {
        SimpleQueryBuilder qb = new SimpleQueryBuilder();
        qb.fromPart.append(new StringBuilder(fromPart.toString()));
        qb.selectPart.append("COUNT(*)");
        qb.whereParts.addAll(whereParts);
        qb.binds.addAll(binds);
        return qb;
    }
}
