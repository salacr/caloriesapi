package org.salac.toptal.task2.auth;

import org.salac.toptal.task2.exception.ForbiddenException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * JWT based authentification filter
 */
public class JWTAuthenticationFilter extends GenericFilterBean {

    private final TokenAuthenticationService authentificationService;

    public JWTAuthenticationFilter(TokenAuthenticationService authentificationService) {
        this.authentificationService = authentificationService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {
            var authentication = authentificationService.getAuthentication((HttpServletRequest) request);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            filterChain.doFilter(request,response);
        }
        catch (ForbiddenException e)
        {
            var httpResponse = (HttpServletResponse) response;
            httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            var writer = httpResponse.getWriter();
            writer.print(e.getMessage());
            writer.close();
        }

    }
}
