package org.salac.toptal.task2.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.salac.toptal.task2.dto.repository.UserLoginRepository;
import org.salac.toptal.task2.exception.ForbiddenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.UUID;

/**
 * Service responsible for handling token authentification
 */
@Component
public class TokenAuthenticationService {

    private static final long EXPIRATION_TIME = 1200_000; // 20 minutes

    /**
     * Might be in config
     */
    private final String SECRET;

    /**
     * HTTP header which stores the token
     */
    public static final String HEADER_NAME = "x-access-token";

    private final UserLoginRepository users;

    public TokenAuthenticationService(@Value("${app.token.string}") String secret, @Autowired UserLoginRepository users) {
        this.users = users;
        this.SECRET = secret;
    }

    /**
     * Creates auth token
     * @param id of user
     * @return JWT token
     */
    public String createJwtToken(UUID id) {

        var claims = Jwts.claims();
        claims.setSubject(id.toString());

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    private Claims parseClaims(String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        }
        catch (Exception e)
        {
            throw new ForbiddenException("Unable to interpret auth token:" + e.getMessage(), e);
        }
    }

    /**
     * Parse Auth token from HTTP request
     * @param request current HTTP request
     * @return jwt token representation
     */
    public Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_NAME);
        if (token != null && !token.isEmpty()) {
            // interpret the token.
            var claims = parseClaims(token);

            var userId = UUID.fromString(claims.getSubject());

            return users.findById(userId).map(userDTO -> userDTO.isLocked() ? null : new JWTAuthenticationToken(
                    userDTO,
                    null
            )).orElse(null);
        }
        return null;
    }
}

