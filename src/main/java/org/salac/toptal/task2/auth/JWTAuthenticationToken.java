package org.salac.toptal.task2.auth;

import org.salac.toptal.task2.dto.UserLogin;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * JWT token used to hold information about logged user
 */
@Configurable
public class JWTAuthenticationToken extends UsernamePasswordAuthenticationToken {

    JWTAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials, Collections.emptySet());
    }

    public UserLogin getUser() {
        return (UserLogin) getPrincipal();
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return Set.of((GrantedAuthority) () -> getUser().getRole().toString());
    }
}

