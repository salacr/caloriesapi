package org.salac.toptal.task2.auth;

import org.springframework.security.core.Authentication;

/**
 * This interface is used to inject Authentication to another beans
 */
public interface AuthFacade {
    Authentication getAuthentication();
}
