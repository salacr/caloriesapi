package org.salac.toptal.task2;

import java.util.Objects;

public class HumanReadableFieldError {

    private String field;

    private String defaultMessage;

    /**
     * Used by jackson
     */
    public HumanReadableFieldError() {
    }

    public HumanReadableFieldError(String field, String defaultMessage) {
        this.field = field;
        this.defaultMessage = defaultMessage;
    }

    public String getField() {
        return field;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HumanReadableFieldError that = (HumanReadableFieldError) o;
        return Objects.equals(field, that.field) &&
                Objects.equals(defaultMessage, that.defaultMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field, defaultMessage);
    }

    @Override
    public String toString() {
        return "HumanReadableFieldError{" +
                "field='" + field + '\'' +
                ", defaultMessage='" + defaultMessage + '\'' +
                '}';
    }
}
