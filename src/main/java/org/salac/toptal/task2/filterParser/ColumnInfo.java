package org.salac.toptal.task2.filterParser;

public class ColumnInfo
{
    public enum COLUMN_TYPE{
        BOOLEAN,
        STRING,
        NUMBER,
        UUID,
        DATE,
        TIME
    }

    private final COLUMN_TYPE columnType;

    private final QueryType queryType;

    private final String dbRepresentation;

    public ColumnInfo(String dbRepresentation, COLUMN_TYPE columnType) {
        this.queryType = dbRepresentation.startsWith("meal") ? QueryType.MEAL : QueryType.USER;
        this.dbRepresentation = dbRepresentation;
        this.columnType = columnType;
    }

    public QueryType getQueryType() {
        return queryType;
    }

    public String getDbRepresentation() {
        return dbRepresentation;
    }

    public COLUMN_TYPE getColumnType() {
        return columnType;
    }
}
