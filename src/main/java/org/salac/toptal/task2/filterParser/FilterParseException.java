package org.salac.toptal.task2.filterParser;

public class FilterParseException extends RuntimeException{

    public FilterParseException(String message) {
        super(message);
    }
}
