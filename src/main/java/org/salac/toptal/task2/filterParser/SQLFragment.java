package org.salac.toptal.task2.filterParser;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SQLFragment {

    private final String sql;

    private final List<Object> binds;

    public SQLFragment() {
        this.sql = "";
        this.binds = new LinkedList<>();
    }

    public SQLFragment(String sql) {
        this.sql = sql;
        this.binds = new LinkedList<>();
    }

    public SQLFragment(String sql, Object bind) {
        this.sql = sql;
        this.binds = Collections.singletonList(bind);
    }

    public SQLFragment(String sql, List<Object> binds) {
        this.sql = sql;
        this.binds = Collections.unmodifiableList(binds);
    }

    public String getSql() {
        return sql;
    }

    public List<Object> getBinds() {
        return binds;
    }

    public boolean isEmpty()
    {
        return sql.isEmpty();
    }
}
