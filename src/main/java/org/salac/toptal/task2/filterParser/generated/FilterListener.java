// Generated from /home/salacr/IdeaProjects/task2/src/main/Filter.g4 by ANTLR 4.7
package org.salac.toptal.task2.filterParser.generated;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link FilterParser}.
 */
public interface FilterListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link FilterParser#filter}.
	 * @param ctx the parse tree
	 */
	void enterFilter(FilterParser.FilterContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#filter}.
	 * @param ctx the parse tree
	 */
	void exitFilter(FilterParser.FilterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code OrBlock}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 */
	void enterOrBlock(FilterParser.OrBlockContext ctx);
	/**
	 * Exit a parse tree produced by the {@code OrBlock}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 */
	void exitOrBlock(FilterParser.OrBlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AndBlock}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 */
	void enterAndBlock(FilterParser.AndBlockContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AndBlock}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 */
	void exitAndBlock(FilterParser.AndBlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ParenExp}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 */
	void enterParenExp(FilterParser.ParenExpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ParenExp}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 */
	void exitParenExp(FilterParser.ParenExpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AtomExp}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 */
	void enterAtomExp(FilterParser.AtomExpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AtomExp}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 */
	void exitAtomExp(FilterParser.AtomExpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BoolTermParenthesisBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 */
	void enterBoolTermParenthesisBlok(FilterParser.BoolTermParenthesisBlokContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BoolTermParenthesisBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 */
	void exitBoolTermParenthesisBlok(FilterParser.BoolTermParenthesisBlokContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NumberAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 */
	void enterNumberAtomBlok(FilterParser.NumberAtomBlokContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NumberAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 */
	void exitNumberAtomBlok(FilterParser.NumberAtomBlokContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StringLikeAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 */
	void enterStringLikeAtomBlok(FilterParser.StringLikeAtomBlokContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StringLikeAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 */
	void exitStringLikeAtomBlok(FilterParser.StringLikeAtomBlokContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BoolAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 */
	void enterBoolAtomBlok(FilterParser.BoolAtomBlokContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BoolAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 */
	void exitBoolAtomBlok(FilterParser.BoolAtomBlokContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#stringlikeatom}.
	 * @param ctx the parse tree
	 */
	void enterStringlikeatom(FilterParser.StringlikeatomContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#stringlikeatom}.
	 * @param ctx the parse tree
	 */
	void exitStringlikeatom(FilterParser.StringlikeatomContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#numberatom}.
	 * @param ctx the parse tree
	 */
	void enterNumberatom(FilterParser.NumberatomContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#numberatom}.
	 * @param ctx the parse tree
	 */
	void exitNumberatom(FilterParser.NumberatomContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#boolatom}.
	 * @param ctx the parse tree
	 */
	void enterBoolatom(FilterParser.BoolatomContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#boolatom}.
	 * @param ctx the parse tree
	 */
	void exitBoolatom(FilterParser.BoolatomContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#bool_column}.
	 * @param ctx the parse tree
	 */
	void enterBool_column(FilterParser.Bool_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#bool_column}.
	 * @param ctx the parse tree
	 */
	void exitBool_column(FilterParser.Bool_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#time_column}.
	 * @param ctx the parse tree
	 */
	void enterTime_column(FilterParser.Time_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#time_column}.
	 * @param ctx the parse tree
	 */
	void exitTime_column(FilterParser.Time_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#date_column}.
	 * @param ctx the parse tree
	 */
	void enterDate_column(FilterParser.Date_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#date_column}.
	 * @param ctx the parse tree
	 */
	void exitDate_column(FilterParser.Date_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#numeric_column}.
	 * @param ctx the parse tree
	 */
	void enterNumeric_column(FilterParser.Numeric_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#numeric_column}.
	 * @param ctx the parse tree
	 */
	void exitNumeric_column(FilterParser.Numeric_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#uuid_column}.
	 * @param ctx the parse tree
	 */
	void enterUuid_column(FilterParser.Uuid_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#uuid_column}.
	 * @param ctx the parse tree
	 */
	void exitUuid_column(FilterParser.Uuid_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link FilterParser#string_column}.
	 * @param ctx the parse tree
	 */
	void enterString_column(FilterParser.String_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link FilterParser#string_column}.
	 * @param ctx the parse tree
	 */
	void exitString_column(FilterParser.String_columnContext ctx);
}