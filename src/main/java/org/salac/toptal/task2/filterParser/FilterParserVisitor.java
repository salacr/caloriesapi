package org.salac.toptal.task2.filterParser;

import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import org.salac.toptal.task2.filterParser.generated.FilterBaseVisitor;
import org.salac.toptal.task2.filterParser.generated.FilterParser;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class FilterParserVisitor extends FilterBaseVisitor<SQLFragment> {

    private final QueryType queryType;

    private final Vocabulary vocabulary;

    private final static DbColumnsConfig mapOfColumns = new DbColumnsConfig();

    public FilterParserVisitor(Vocabulary vocabulary, QueryType queryType) {
        super();
        this.vocabulary = vocabulary;
        this.queryType = queryType;

    }

    @Override
    protected SQLFragment aggregateResult(SQLFragment aggregate, SQLFragment nextResult) {
        String separator = " ";
        if (aggregate.isEmpty() || nextResult.isEmpty())
        {
            separator = "";
        }

        var binds = new ArrayList<>(aggregate.getBinds().size()  + nextResult.getBinds().size());
        binds.addAll(aggregate.getBinds());
        binds.addAll(nextResult.getBinds());
        return new SQLFragment(aggregate.getSql() + separator + nextResult.getSql(), binds);
    }

    @Override
    protected SQLFragment defaultResult() {
        return new SQLFragment();
    }

    @Override
    public SQLFragment visitFilter(FilterParser.FilterContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public SQLFragment visitStringLikeAtomBlok(FilterParser.StringLikeAtomBlokContext ctx)
    {
        var realType = determineRealType(ctx);
        if (realType == ColumnInfo.COLUMN_TYPE.DATE || realType == ColumnInfo.COLUMN_TYPE.TIME || realType == ColumnInfo.COLUMN_TYPE.UUID)
        {
            ColumnInfo.COLUMN_TYPE type = getStringlikeRealTypeForChild(ctx.getChild(0));
            if (type == null)
            {
                validateTextValueForNode(ctx.getChild(0), realType);
            }
            type = getStringlikeRealTypeForChild(ctx.getChild(2));
            if (type == null)
            {
                validateTextValueForNode(ctx.getChild(2), realType);
            }
        }

        return visitChildren(ctx);
    }

    private void validateTextValueForNode(ParseTree tree, ColumnInfo.COLUMN_TYPE realType)
    {
        String value = trimStringQuotes(((TerminalNode) tree.getChild(0)).getSymbol().getText());
        if (realType == ColumnInfo.COLUMN_TYPE.DATE)
        {
            try {
                LocalDate.parse(value);
            }
            catch (Exception e)
            {
                throw new FilterParseException("Wrong value '" + value + "' expecting date in format 'yyyy-mm-dd' ");
            }
        }
        else if (realType == ColumnInfo.COLUMN_TYPE.TIME)
        {
            try {
                LocalTime.parse(value);
            }
            catch (Exception e)
            {
                throw new FilterParseException("Wrong value '" + value + "' expecting time in format 'hh:mm:ss' ");
            }
        }
        else if (realType == ColumnInfo.COLUMN_TYPE.UUID)
        {
            try {
                UUID.fromString(value);
            }
            catch (Exception e)
            {
                throw new FilterParseException("Wrong value '" + value + "' expecting UUIDv4 format");
            }
        }
    }

    private ColumnInfo.COLUMN_TYPE determineRealType(FilterParser.StringLikeAtomBlokContext ctx) {
        ColumnInfo.COLUMN_TYPE type = getStringlikeRealTypeForChild(ctx.getChild(0));
        if (type != null)
        {
            return type;
        }

        type = getStringlikeRealTypeForChild(ctx.getChild(2));
        if (type != null)
        {
            return type;
        }
        return ColumnInfo.COLUMN_TYPE.STRING;
    }

    private ColumnInfo.COLUMN_TYPE getStringlikeRealTypeForChild(ParseTree tree) {
        var firstChild = tree.getChild(0);
        if (firstChild.getChildCount() > 0)
        {
            var text = firstChild.getChild(0).getText();
            var columnInfo = mapOfColumns.get(text);
            if (columnInfo != null)
            {
                return columnInfo.getColumnType();
            }
        }
        return null;
    }

    public SQLFragment visitTerminal(TerminalNode node) {
        String type = vocabulary.getSymbolicName(node.getSymbol().getType());
        if (type != null) {
            switch (type) {
                case "LPAREN":
                    return new SQLFragment("(");
                case "RPAREN":
                    return new SQLFragment(")");
                case "AND":
                    return new SQLFragment("AND");
                case "OR":
                    return new SQLFragment("OR");
                case "EQ":
                    return new SQLFragment("=");
                case "NE":
                    return new SQLFragment("!=");
                case "GT":
                    return new SQLFragment(">");
                case "LT":
                    return new SQLFragment("<");
                case "BOOL":
                    return new SQLFragment(node.getText());
                case "STRING":
                    var rawTextValue = trimStringQuotes(node.getText());
                    try {
                        var stringLikeAtomBlokContext = node.getParent().getParent();
                        var realType = determineRealType((FilterParser.StringLikeAtomBlokContext) stringLikeAtomBlokContext);
                        if (realType == ColumnInfo.COLUMN_TYPE.UUID) {
                            return new SQLFragment("?", UUID.fromString(rawTextValue));
                        } else if (realType == ColumnInfo.COLUMN_TYPE.DATE) {
                            return new SQLFragment("?", LocalDate.parse(rawTextValue));
                        } else if (realType == ColumnInfo.COLUMN_TYPE.TIME) {
                            return new SQLFragment("?", LocalTime.parse(rawTextValue));
                        } else {
                            return new SQLFragment("?", rawTextValue);
                        }
                    } catch (Exception e) {
                        return new SQLFragment("?", rawTextValue);
                    }
                case "NUMBER":
                    return new SQLFragment("?", new BigDecimal(node.getText()));
                case "EOF":
                    return new SQLFragment();
                default:
                    throw new FilterParseException("Problem with parsing token:" + node.getText());
            }
        }
        else{
            return translateColumnExpressions(node.getText());
        }
    }

    private String trimStringQuotes(String text) {
        return text.substring(1, text.length() - 1);
    }

    private SQLFragment translateColumnExpressions(String text) {

        var columnInfo = mapOfColumns.get(text);
        if (columnInfo == null)
        {
            throw new FilterParseException("Problem with translating column:" + text + " you can use one of: " + String.join(", ", mapOfColumns.getListOfEligibleFieldsForQueryType(queryType)));
        }
        if (columnInfo.getQueryType() != queryType)
        {
            throw new FilterParseException("This report don't contain given field:" + text + " you can use one of: " + String.join(", ", mapOfColumns.getListOfEligibleFieldsForQueryType(queryType)));
        }

        return new SQLFragment(columnInfo.getDbRepresentation());
    }

}
