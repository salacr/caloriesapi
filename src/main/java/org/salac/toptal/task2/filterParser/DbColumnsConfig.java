package org.salac.toptal.task2.filterParser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DbColumnsConfig {

    private final static Map<String, ColumnInfo> mapOfColumns = new HashMap<>();

    public DbColumnsConfig()
    {
        if (mapOfColumns.isEmpty()) {
            initializeMapOfColumns();
        }
    }

    private void initializeMapOfColumns() {
        mapOfColumns.put("user.email", new ColumnInfo("user_login.email", ColumnInfo.COLUMN_TYPE.STRING));
        mapOfColumns.put("user.id", new ColumnInfo("user_login.id", ColumnInfo.COLUMN_TYPE.UUID));
        mapOfColumns.put("user.isLocked", new ColumnInfo("user_login.is_locked", ColumnInfo.COLUMN_TYPE.BOOLEAN));
        mapOfColumns.put("user.emailVerified", new ColumnInfo("user_login.email_verified", ColumnInfo.COLUMN_TYPE.BOOLEAN));
        mapOfColumns.put("user.firstName", new ColumnInfo("user_login.first_name", ColumnInfo.COLUMN_TYPE.STRING));
        mapOfColumns.put("user.lastName", new ColumnInfo("user_login.last_name", ColumnInfo.COLUMN_TYPE.STRING));
        mapOfColumns.put("user.role", new ColumnInfo("user_login.role", ColumnInfo.COLUMN_TYPE.STRING));
        mapOfColumns.put("user.badPasswordsInRow", new ColumnInfo("user_login.bad_login_atempts_in_row", ColumnInfo.COLUMN_TYPE.NUMBER));
        mapOfColumns.put("user.calloriesPerDay", new ColumnInfo("user_login.calories_per_day", ColumnInfo.COLUMN_TYPE.NUMBER));

        mapOfColumns.put("meal.id", new ColumnInfo("meal.id", ColumnInfo.COLUMN_TYPE.UUID));
        mapOfColumns.put("meal.userid", new ColumnInfo("meal.user_login_id", ColumnInfo.COLUMN_TYPE.UUID));
        mapOfColumns.put("meal.date", new ColumnInfo("meal.date", ColumnInfo.COLUMN_TYPE.DATE));
        mapOfColumns.put("meal.time", new ColumnInfo("meal.time", ColumnInfo.COLUMN_TYPE.TIME));
        mapOfColumns.put("meal.calories", new ColumnInfo("meal.calories", ColumnInfo.COLUMN_TYPE.NUMBER));
        mapOfColumns.put("meal.text", new ColumnInfo("meal.text", ColumnInfo.COLUMN_TYPE.STRING));
        mapOfColumns.put("meal.isLessCaloriesThenExpected", new ColumnInfo("meal.is_less_then_expected", ColumnInfo.COLUMN_TYPE.BOOLEAN));
    }

    public ColumnInfo get(String text) {
        return mapOfColumns.get(text);
    }

    public List<String> getListOfEligibleFieldsForQueryType(QueryType queryType) {
        List<String> possibleNames = new LinkedList<>();
        mapOfColumns.forEach((name, columnInfo) -> {
            if (columnInfo.getQueryType() == queryType)
            {
                possibleNames.add(name);
            }
        });
        return possibleNames;
    }
}
