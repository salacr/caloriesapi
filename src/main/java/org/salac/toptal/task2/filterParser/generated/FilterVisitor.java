// Generated from /home/salacr/IdeaProjects/task2/src/main/Filter.g4 by ANTLR 4.7
package org.salac.toptal.task2.filterParser.generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link FilterParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface FilterVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link FilterParser#filter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilter(FilterParser.FilterContext ctx);
	/**
	 * Visit a parse tree produced by the {@code OrBlock}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrBlock(FilterParser.OrBlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code AndBlock}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndBlock(FilterParser.AndBlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParenExp}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExp(FilterParser.ParenExpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code AtomExp}
	 * labeled alternative in {@link FilterParser#boolexpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomExp(FilterParser.AtomExpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolTermParenthesisBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolTermParenthesisBlok(FilterParser.BoolTermParenthesisBlokContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NumberAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberAtomBlok(FilterParser.NumberAtomBlokContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StringLikeAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLikeAtomBlok(FilterParser.StringLikeAtomBlokContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolAtomBlok}
	 * labeled alternative in {@link FilterParser#boolterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolAtomBlok(FilterParser.BoolAtomBlokContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#stringlikeatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringlikeatom(FilterParser.StringlikeatomContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#numberatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberatom(FilterParser.NumberatomContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#boolatom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolatom(FilterParser.BoolatomContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#bool_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool_column(FilterParser.Bool_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#time_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTime_column(FilterParser.Time_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#date_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDate_column(FilterParser.Date_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#numeric_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumeric_column(FilterParser.Numeric_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#uuid_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUuid_column(FilterParser.Uuid_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link FilterParser#string_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString_column(FilterParser.String_columnContext ctx);
}