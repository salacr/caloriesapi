package org.salac.toptal.task2.filterParser;

import org.antlr.v4.runtime.*;
import org.salac.toptal.task2.filterParser.generated.FilterLexer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


public class FilterInterpret {


    public FilterInterpret() {

    }

    public SQLFragment interpret(String input, QueryType queryType)
    {
        try {
            input = input.trim();
            if (input.isEmpty()) {
                return new SQLFragment("");
            }

            var stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
            var filterLexer = new FilterLexer(CharStreams.fromStream(stream, StandardCharsets.UTF_8));
            var commonTokenStream = new CommonTokenStream(filterLexer);
            var filterParser = new org.salac.toptal.task2.filterParser.generated.FilterParser(commonTokenStream);
            filterParser.addErrorListener(new ParserErrorListener());
            filterParser.removeErrorListener(ConsoleErrorListener.INSTANCE);


            var vocabulary = filterLexer.getVocabulary();

            var filterContext = filterParser.filter();

            var visitor = new FilterParserVisitor(vocabulary, queryType);
            return visitor.visitFilter(filterContext);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Unable to parse input:" + input);
        }
    }
}
