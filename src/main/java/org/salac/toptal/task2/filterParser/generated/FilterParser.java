// Generated from /home/salacr/IdeaProjects/task2/src/main/Filter.g4 by ANTLR 4.7
package org.salac.toptal.task2.filterParser.generated;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FilterParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, BOOL=17, 
		EQ=18, NE=19, GT=20, LT=21, STRING=22, NUMBER=23, DIGITS=24, AND=25, OR=26, 
		LPAREN=27, RPAREN=28, WS=29;
	public static final int
		RULE_filter = 0, RULE_boolexpression = 1, RULE_boolterm = 2, RULE_stringlikeatom = 3, 
		RULE_numberatom = 4, RULE_boolatom = 5, RULE_bool_column = 6, RULE_time_column = 7, 
		RULE_date_column = 8, RULE_numeric_column = 9, RULE_uuid_column = 10, 
		RULE_string_column = 11;
	public static final String[] ruleNames = {
		"filter", "boolexpression", "boolterm", "stringlikeatom", "numberatom", 
		"boolatom", "bool_column", "time_column", "date_column", "numeric_column", 
		"uuid_column", "string_column"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'user.isLocked'", "'user.emailVerified'", "'meal.isLessCaloriesThenExpected'", 
		"'meal.time'", "'meal.date'", "'meal.calories'", "'user.badPasswordsInRow'", 
		"'user.calloriesPerDay'", "'meal.id'", "'meal.userid'", "'user.id'", "'meal.text'", 
		"'user.firstName'", "'user.lastName'", "'user.email'", "'user.role'", 
		null, "'eq'", "'ne'", "'gt'", "'lt'", null, null, null, null, null, "'('", 
		"')'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "BOOL", "EQ", "NE", "GT", "LT", "STRING", 
		"NUMBER", "DIGITS", "AND", "OR", "LPAREN", "RPAREN", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Filter.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public FilterParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class FilterContext extends ParserRuleContext {
		public BoolexpressionContext boolexpression() {
			return getRuleContext(BoolexpressionContext.class,0);
		}
		public TerminalNode EOF() { return getToken(FilterParser.EOF, 0); }
		public FilterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterFilter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitFilter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitFilter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FilterContext filter() throws RecognitionException {
		FilterContext _localctx = new FilterContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_filter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(24);
			boolexpression(0);
			setState(25);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolexpressionContext extends ParserRuleContext {
		public BoolexpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolexpression; }
	 
		public BoolexpressionContext() { }
		public void copyFrom(BoolexpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OrBlockContext extends BoolexpressionContext {
		public List<BoolexpressionContext> boolexpression() {
			return getRuleContexts(BoolexpressionContext.class);
		}
		public BoolexpressionContext boolexpression(int i) {
			return getRuleContext(BoolexpressionContext.class,i);
		}
		public TerminalNode OR() { return getToken(FilterParser.OR, 0); }
		public OrBlockContext(BoolexpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterOrBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitOrBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitOrBlock(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndBlockContext extends BoolexpressionContext {
		public List<BoolexpressionContext> boolexpression() {
			return getRuleContexts(BoolexpressionContext.class);
		}
		public BoolexpressionContext boolexpression(int i) {
			return getRuleContext(BoolexpressionContext.class,i);
		}
		public TerminalNode AND() { return getToken(FilterParser.AND, 0); }
		public AndBlockContext(BoolexpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterAndBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitAndBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitAndBlock(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenExpContext extends BoolexpressionContext {
		public TerminalNode LPAREN() { return getToken(FilterParser.LPAREN, 0); }
		public BoolexpressionContext boolexpression() {
			return getRuleContext(BoolexpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FilterParser.RPAREN, 0); }
		public ParenExpContext(BoolexpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterParenExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitParenExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitParenExp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AtomExpContext extends BoolexpressionContext {
		public BooltermContext boolterm() {
			return getRuleContext(BooltermContext.class,0);
		}
		public AtomExpContext(BoolexpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterAtomExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitAtomExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitAtomExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolexpressionContext boolexpression() throws RecognitionException {
		return boolexpression(0);
	}

	private BoolexpressionContext boolexpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BoolexpressionContext _localctx = new BoolexpressionContext(_ctx, _parentState);
		BoolexpressionContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_boolexpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(33);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				{
				_localctx = new ParenExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(28);
				match(LPAREN);
				setState(29);
				boolexpression(0);
				setState(30);
				match(RPAREN);
				}
				break;
			case 2:
				{
				_localctx = new AtomExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(32);
				boolterm();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(43);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(41);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
					case 1:
						{
						_localctx = new AndBlockContext(new BoolexpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolexpression);
						setState(35);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(36);
						match(AND);
						setState(37);
						boolexpression(4);
						}
						break;
					case 2:
						{
						_localctx = new OrBlockContext(new BoolexpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_boolexpression);
						setState(38);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(39);
						match(OR);
						setState(40);
						boolexpression(3);
						}
						break;
					}
					} 
				}
				setState(45);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BooltermContext extends ParserRuleContext {
		public BooltermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolterm; }
	 
		public BooltermContext() { }
		public void copyFrom(BooltermContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NumberAtomBlokContext extends BooltermContext {
		public List<NumberatomContext> numberatom() {
			return getRuleContexts(NumberatomContext.class);
		}
		public NumberatomContext numberatom(int i) {
			return getRuleContext(NumberatomContext.class,i);
		}
		public TerminalNode EQ() { return getToken(FilterParser.EQ, 0); }
		public TerminalNode NE() { return getToken(FilterParser.NE, 0); }
		public TerminalNode GT() { return getToken(FilterParser.GT, 0); }
		public TerminalNode LT() { return getToken(FilterParser.LT, 0); }
		public NumberAtomBlokContext(BooltermContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterNumberAtomBlok(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitNumberAtomBlok(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitNumberAtomBlok(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolAtomBlokContext extends BooltermContext {
		public List<BoolatomContext> boolatom() {
			return getRuleContexts(BoolatomContext.class);
		}
		public BoolatomContext boolatom(int i) {
			return getRuleContext(BoolatomContext.class,i);
		}
		public TerminalNode EQ() { return getToken(FilterParser.EQ, 0); }
		public TerminalNode NE() { return getToken(FilterParser.NE, 0); }
		public BoolAtomBlokContext(BooltermContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterBoolAtomBlok(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitBoolAtomBlok(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitBoolAtomBlok(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolTermParenthesisBlokContext extends BooltermContext {
		public TerminalNode LPAREN() { return getToken(FilterParser.LPAREN, 0); }
		public BooltermContext boolterm() {
			return getRuleContext(BooltermContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FilterParser.RPAREN, 0); }
		public BoolTermParenthesisBlokContext(BooltermContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterBoolTermParenthesisBlok(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitBoolTermParenthesisBlok(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitBoolTermParenthesisBlok(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringLikeAtomBlokContext extends BooltermContext {
		public List<StringlikeatomContext> stringlikeatom() {
			return getRuleContexts(StringlikeatomContext.class);
		}
		public StringlikeatomContext stringlikeatom(int i) {
			return getRuleContext(StringlikeatomContext.class,i);
		}
		public TerminalNode EQ() { return getToken(FilterParser.EQ, 0); }
		public TerminalNode NE() { return getToken(FilterParser.NE, 0); }
		public TerminalNode GT() { return getToken(FilterParser.GT, 0); }
		public TerminalNode LT() { return getToken(FilterParser.LT, 0); }
		public StringLikeAtomBlokContext(BooltermContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterStringLikeAtomBlok(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitStringLikeAtomBlok(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitStringLikeAtomBlok(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooltermContext boolterm() throws RecognitionException {
		BooltermContext _localctx = new BooltermContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_boolterm);
		int _la;
		try {
			setState(62);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LPAREN:
				_localctx = new BoolTermParenthesisBlokContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(46);
				match(LPAREN);
				setState(47);
				boolterm();
				setState(48);
				match(RPAREN);
				}
				break;
			case T__5:
			case T__6:
			case T__7:
			case NUMBER:
				_localctx = new NumberAtomBlokContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(50);
				numberatom();
				setState(51);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << NE) | (1L << GT) | (1L << LT))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(52);
				numberatom();
				}
				break;
			case T__3:
			case T__4:
			case T__8:
			case T__9:
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case STRING:
				_localctx = new StringLikeAtomBlokContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(54);
				stringlikeatom();
				setState(55);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << NE) | (1L << GT) | (1L << LT))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(56);
				stringlikeatom();
				}
				break;
			case T__0:
			case T__1:
			case T__2:
			case BOOL:
				_localctx = new BoolAtomBlokContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(58);
				boolatom();
				setState(59);
				_la = _input.LA(1);
				if ( !(_la==EQ || _la==NE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(60);
				boolatom();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringlikeatomContext extends ParserRuleContext {
		public Date_columnContext date_column() {
			return getRuleContext(Date_columnContext.class,0);
		}
		public Time_columnContext time_column() {
			return getRuleContext(Time_columnContext.class,0);
		}
		public String_columnContext string_column() {
			return getRuleContext(String_columnContext.class,0);
		}
		public Uuid_columnContext uuid_column() {
			return getRuleContext(Uuid_columnContext.class,0);
		}
		public TerminalNode STRING() { return getToken(FilterParser.STRING, 0); }
		public StringlikeatomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringlikeatom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterStringlikeatom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitStringlikeatom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitStringlikeatom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringlikeatomContext stringlikeatom() throws RecognitionException {
		StringlikeatomContext _localctx = new StringlikeatomContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_stringlikeatom);
		try {
			setState(69);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(64);
				date_column();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(65);
				time_column();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(66);
				string_column();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(67);
				uuid_column();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(68);
				match(STRING);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberatomContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(FilterParser.NUMBER, 0); }
		public Numeric_columnContext numeric_column() {
			return getRuleContext(Numeric_columnContext.class,0);
		}
		public NumberatomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numberatom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterNumberatom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitNumberatom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitNumberatom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberatomContext numberatom() throws RecognitionException {
		NumberatomContext _localctx = new NumberatomContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_numberatom);
		try {
			setState(73);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(71);
				match(NUMBER);
				}
				break;
			case T__5:
			case T__6:
			case T__7:
				enterOuterAlt(_localctx, 2);
				{
				setState(72);
				numeric_column();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolatomContext extends ParserRuleContext {
		public TerminalNode BOOL() { return getToken(FilterParser.BOOL, 0); }
		public Bool_columnContext bool_column() {
			return getRuleContext(Bool_columnContext.class,0);
		}
		public BoolatomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolatom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterBoolatom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitBoolatom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitBoolatom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolatomContext boolatom() throws RecognitionException {
		BoolatomContext _localctx = new BoolatomContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_boolatom);
		try {
			setState(77);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BOOL:
				enterOuterAlt(_localctx, 1);
				{
				setState(75);
				match(BOOL);
				}
				break;
			case T__0:
			case T__1:
			case T__2:
				enterOuterAlt(_localctx, 2);
				{
				setState(76);
				bool_column();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bool_columnContext extends ParserRuleContext {
		public Bool_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterBool_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitBool_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitBool_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bool_columnContext bool_column() throws RecognitionException {
		Bool_columnContext _localctx = new Bool_columnContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_bool_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Time_columnContext extends ParserRuleContext {
		public Time_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_time_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterTime_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitTime_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitTime_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Time_columnContext time_column() throws RecognitionException {
		Time_columnContext _localctx = new Time_columnContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_time_column);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Date_columnContext extends ParserRuleContext {
		public Date_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_date_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterDate_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitDate_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitDate_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Date_columnContext date_column() throws RecognitionException {
		Date_columnContext _localctx = new Date_columnContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_date_column);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(83);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Numeric_columnContext extends ParserRuleContext {
		public Numeric_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numeric_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterNumeric_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitNumeric_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitNumeric_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Numeric_columnContext numeric_column() throws RecognitionException {
		Numeric_columnContext _localctx = new Numeric_columnContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_numeric_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__6) | (1L << T__7))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Uuid_columnContext extends ParserRuleContext {
		public Uuid_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_uuid_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterUuid_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitUuid_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitUuid_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Uuid_columnContext uuid_column() throws RecognitionException {
		Uuid_columnContext _localctx = new Uuid_columnContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_uuid_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__8) | (1L << T__9) | (1L << T__10))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class String_columnContext extends ParserRuleContext {
		public String_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).enterString_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FilterListener ) ((FilterListener)listener).exitString_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilterVisitor ) return ((FilterVisitor<? extends T>)visitor).visitString_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final String_columnContext string_column() throws RecognitionException {
		String_columnContext _localctx = new String_columnContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_string_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__8) | (1L << T__9) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return boolexpression_sempred((BoolexpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean boolexpression_sempred(BoolexpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 3);
		case 1:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\37^\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\4\r\t\r\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\5\3$\n\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\7\3,\n\3\f\3\16\3/\13\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4A\n\4\3\5\3\5\3\5\3\5\3\5\5\5H\n\5"+
		"\3\6\3\6\5\6L\n\6\3\7\3\7\5\7P\n\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3"+
		"\f\3\f\3\r\3\r\3\r\2\3\4\16\2\4\6\b\n\f\16\20\22\24\26\30\2\b\3\2\24\27"+
		"\3\2\24\25\3\2\3\5\3\2\b\n\3\2\13\r\4\2\13\f\16\22\2]\2\32\3\2\2\2\4#"+
		"\3\2\2\2\6@\3\2\2\2\bG\3\2\2\2\nK\3\2\2\2\fO\3\2\2\2\16Q\3\2\2\2\20S\3"+
		"\2\2\2\22U\3\2\2\2\24W\3\2\2\2\26Y\3\2\2\2\30[\3\2\2\2\32\33\5\4\3\2\33"+
		"\34\7\2\2\3\34\3\3\2\2\2\35\36\b\3\1\2\36\37\7\35\2\2\37 \5\4\3\2 !\7"+
		"\36\2\2!$\3\2\2\2\"$\5\6\4\2#\35\3\2\2\2#\"\3\2\2\2$-\3\2\2\2%&\f\5\2"+
		"\2&\'\7\33\2\2\',\5\4\3\6()\f\4\2\2)*\7\34\2\2*,\5\4\3\5+%\3\2\2\2+(\3"+
		"\2\2\2,/\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\5\3\2\2\2/-\3\2\2\2\60\61\7\35\2"+
		"\2\61\62\5\6\4\2\62\63\7\36\2\2\63A\3\2\2\2\64\65\5\n\6\2\65\66\t\2\2"+
		"\2\66\67\5\n\6\2\67A\3\2\2\289\5\b\5\29:\t\2\2\2:;\5\b\5\2;A\3\2\2\2<"+
		"=\5\f\7\2=>\t\3\2\2>?\5\f\7\2?A\3\2\2\2@\60\3\2\2\2@\64\3\2\2\2@8\3\2"+
		"\2\2@<\3\2\2\2A\7\3\2\2\2BH\5\22\n\2CH\5\20\t\2DH\5\30\r\2EH\5\26\f\2"+
		"FH\7\30\2\2GB\3\2\2\2GC\3\2\2\2GD\3\2\2\2GE\3\2\2\2GF\3\2\2\2H\t\3\2\2"+
		"\2IL\7\31\2\2JL\5\24\13\2KI\3\2\2\2KJ\3\2\2\2L\13\3\2\2\2MP\7\23\2\2N"+
		"P\5\16\b\2OM\3\2\2\2ON\3\2\2\2P\r\3\2\2\2QR\t\4\2\2R\17\3\2\2\2ST\7\6"+
		"\2\2T\21\3\2\2\2UV\7\7\2\2V\23\3\2\2\2WX\t\5\2\2X\25\3\2\2\2YZ\t\6\2\2"+
		"Z\27\3\2\2\2[\\\t\7\2\2\\\31\3\2\2\2\t#+-@GKO";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}