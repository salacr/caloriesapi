package org.salac.toptal.task2.controller;

import org.junit.runner.RunWith;
import org.salac.toptal.task2.CleanDbListener;
import org.salac.toptal.task2.auth.TokenAuthenticationService;
import org.salac.toptal.task2.dto.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@TestExecutionListeners(
        listeners = CleanDbListener.class,
        mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
)
public class AbstractTest {

    @Autowired
    private InvitationTokenRepository invitationTokenRepository;

    @Autowired
    private EmailVerificationTokenRepository verificationTokenRepository;

    @Autowired
    private UserLoginRepository userLoginRepository;

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @Autowired
    private LostPasswordTokenRepository lostPasswordRepository;

    @Autowired
    private JdbcTemplate jdbc;

    @Value("${app.servername}")
    private String serverName;

    private HttpHeaders createHeaders(String token){
        HttpHeaders headers = new HttpHeaders();
        if (token != null && !token.isEmpty()) {
            headers.set(TokenAuthenticationService.HEADER_NAME, token);
        }
        return headers;
    }


    private <T> ResponseEntity<T> doRequestForEntity(HttpMethod method, String url, String token, Object request, Class<T> responseType) {
        HttpEntity<?> httpEntity = new HttpEntity<>(request, createHeaders(token));
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    protected <T> ResponseEntity<T> postRequest(String url, Object request, Class<T> responseType, Object... urlVariables)
    {
        return doRequestForEntity(HttpMethod.POST, url, null, request, responseType);
    }

    protected <T> ResponseEntity<T> postRequest(String url, String token, Object request, Class<T> responseType, Object... urlVariables)
    {
        return doRequestForEntity(HttpMethod.POST, url, token, request, responseType);
    }

    protected <T> ResponseEntity<T> postRequest(String url, String token, Class<T> responseType, Object... urlVariables)
    {
        return doRequestForEntity(HttpMethod.POST, url, token, null, responseType);
    }

    protected <T> ResponseEntity<T> putRequest(String url, String token, Object request, Class<T> responseType, Object... urlVariables)
    {
        return doRequestForEntity(HttpMethod.PUT, url, token, request, responseType);
    }

    protected <T> ResponseEntity<T> getRequest(String url, String token, Class<T> responseType, Object... urlVariables)
    {
        return doRequestForEntity(HttpMethod.GET, url, token, null, responseType);
    }

    protected <T> ResponseEntity<T> getRequest(String url, Object request, Class<T> responseType, Object... urlVariables)
    {
        return doRequestForEntity(HttpMethod.GET, url, null, request, responseType);
    }

    protected <T> ResponseEntity<T> getRequest(String url, String token, Object request, Class<T> responseType, Object... urlVariables)
    {
        return doRequestForEntity(HttpMethod.GET, url, token, request, responseType);
    }

    protected <T> ResponseEntity<T> deleteRequest(String url, String token, Class<T> responseType, Object... urlVariables)
    {
        return doRequestForEntity(HttpMethod.DELETE, url, token, null, responseType);
    }

    protected InvitationTokenRepository getInvitationTokenRepository() {
        return invitationTokenRepository;
    }

    protected EmailVerificationTokenRepository getVerificationTokenRepository() {
        return verificationTokenRepository;
    }

    protected UserLoginRepository getUserLoginRepository() {
        return userLoginRepository;
    }

    protected PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }

    protected String createAuthTokenFor(String email) {
        return tokenAuthenticationService.createJwtToken(
            getUserLoginRepository().findByEmail(email).get().getId()
        );
    }

    protected MealRepository getMealRepository() {
        return mealRepository;
    }

    protected RefreshTokenRepository getRefreshTokenRepository() {
        return refreshTokenRepository;
    }

    protected JdbcTemplate getJdbc() {
        return jdbc;
    }

    protected String getServerName() {
        return serverName;
    }

    protected LostPasswordTokenRepository getLostPasswordRepository() {
        return lostPasswordRepository;
    }
}
