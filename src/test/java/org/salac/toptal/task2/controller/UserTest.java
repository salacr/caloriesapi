package org.salac.toptal.task2.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.salac.toptal.task2.HumanReadableFieldError;
import org.salac.toptal.task2.ValidationFailure;
import org.salac.toptal.task2.controller.user.ChangeRoleRequest;
import org.salac.toptal.task2.controller.user.InviteRequest;
import org.salac.toptal.task2.controller.user.UpdateUserRequest;
import org.salac.toptal.task2.dto.Role;
import org.salac.toptal.task2.dto.UserLogin;
import org.salac.toptal.task2.dto.service.UserListResponse;
import org.salac.toptal.task2.dto.service.UserRow;
import org.salac.toptal.task2.mail.EmailChangedNotificationEmail;
import org.salac.toptal.task2.mail.EmailChangedVerificationEmail;
import org.salac.toptal.task2.mail.InvitationEmail;
import org.salac.toptal.task2.mail.NewPasswordWasSetEmail;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class UserTest extends AbstractTest{

    @SpyBean
    private MailSender mailSender;

    @Before
    public void setupMock() {
        doNothing().when(mailSender).send(any(SimpleMailMessage.class));
    }

    @After
    public void resetMock() {
        Mockito.reset(mailSender);
    }

    @Test
    public void simpleListMealsByAdmin()
    {
        var authToken = createAuthTokenFor("admin@example.com");


        var listMealRawResponse = getRequest("/user?page=0&size=2&sort=user.email,desc&calcCount=true", authToken, UserListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        var responseBody = listMealRawResponse.getBody();

        Assert.assertEquals(2, responseBody.getData().length );
        Assert.assertArrayEquals(new UserRow[]{
                new UserRow(UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), "FooUser", "BarUser", "user@example.com",	Role.ROLE_USER, true, 0, false, new BigDecimal("1000.00")),
                new UserRow(UUID.fromString("de600d24-ffea-42d3-bf13-1e911fcfecea"), "FooManager", "BarManager", "manager@example.com",	Role.ROLE_MANAGER, true, 0, false, new BigDecimal("2000.00")),
        }, responseBody.getData() );
        Assert.assertEquals(Long.valueOf(3), responseBody.getCount());
        Assert.assertTrue(responseBody.isNext());

    }

    @Test
    public void simpleListMealsByAdminWithoutNext()
    {
        var authToken = createAuthTokenFor("admin@example.com");


        var listMealRawResponse = getRequest("/user?page=0&size=2&sort=user.email,desc&calcCount=true&checkNext=false", authToken, UserListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        var responseBody = listMealRawResponse.getBody();

        Assert.assertEquals(2, responseBody.getData().length );
        Assert.assertArrayEquals(new UserRow[]{
                new UserRow(UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), "FooUser", "BarUser", "user@example.com",	Role.ROLE_USER, true, 0, false, new BigDecimal("1000.00")),
                new UserRow(UUID.fromString("de600d24-ffea-42d3-bf13-1e911fcfecea"), "FooManager", "BarManager", "manager@example.com",	Role.ROLE_MANAGER, true, 0, false, new BigDecimal("2000.00")),
        }, responseBody.getData() );
        Assert.assertEquals(Long.valueOf(3), responseBody.getCount());
        Assert.assertNull(responseBody.isNext());

    }

    @Test
    public void update_Missing() {
        var authToken = createAuthTokenFor("user@example.com");

        var updateRequest = putRequest("/user/" + UUID.randomUUID(),
                authToken,
                new UpdateUserRequest("new@example.com", "new", "new", "Password", BigDecimal.ONE),
                String.class
        );
        Assert.assertEquals(HttpStatus.NOT_FOUND, updateRequest.getStatusCode());
        Assert.assertEquals("Requested entity can't be found. Probably wrong ID", updateRequest.getBody());
    }

    @Test
    public void update_userTryiesToUpdateAdminAccount() {
        var authToken = createAuthTokenFor("user@example.com");

        var adminId = getUserLoginRepository().getByEmail("admin@example.com").getId();

        var updateRequest = putRequest("/user/" + adminId,
                authToken,
                new UpdateUserRequest("new@example.com", "new", "new", "Password", BigDecimal.ONE),
                Object.class
        );
        Assert.assertEquals(HttpStatus.FORBIDDEN, updateRequest.getStatusCode());
    }

    @Test
    public void update_WrongRequest() {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var updateRequest = putRequest("/user/" + userId,
                authToken,
                new UpdateUserRequest("foooo", "", "new", "Password", BigDecimal.valueOf(-1)),
                ValidationFailure.class
        );
        Assert.assertEquals(HttpStatus.BAD_REQUEST, updateRequest.getStatusCode());
        Assert.assertEquals(
                Set.of(
                    new HumanReadableFieldError("email", "must be a well-formed email address"),
                    new HumanReadableFieldError("firstName", "size must be between 1 and 255"),
                    new HumanReadableFieldError("password", "Password must contain at least 8 characters, including 1 uppercase letter, 1 lowercase letter, and 1 number."),
                    new HumanReadableFieldError("caloriesPerDay", "must be greater than or equal to 0")
                ),
                updateRequest.getBody().getFieldErrors()
        );
    }

    @Test
    public void update_updateToExistingEmailAddress() {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var updateRequest = putRequest("/user/" + userId,
                authToken,
                new UpdateUserRequest("admin@example.com", "Foo", "Bar", "Password1", BigDecimal.valueOf(1)),
                ValidationFailure.class
        );
        Assert.assertEquals(HttpStatus.BAD_REQUEST, updateRequest.getStatusCode());
        Assert.assertEquals(
                Set.of(
                        new HumanReadableFieldError("email", "UserLogin with this e-mail already exists.")
                ),
                updateRequest.getBody().getFieldErrors()
        );
    }

    @Test
    @SqlGroup(
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements =
                    "UPDATE user_login SET email = 'user@example.com', email_verified = true, calories_per_day = 10, first_name = 'FooUser', last_name = 'BarUser', password = '$2a$10$21zhs.cyJGMLEMGvShqB4.EvqsKWhLwllPmrhkn.2kwYLlsywE8oe' WHERE email = 'newuser@example.com'"
            )
    )
    public void update_updateToNewEmailAddress() {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var updateRequest = putRequest("/user/" + userId,
                authToken,
                new UpdateUserRequest("newuser@example.com", "Foo1", "Bar2", null, BigDecimal.valueOf(1000)),
                Object.class
        );
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateRequest.getStatusCode());

        var user = getUserLoginRepository().getByEmail("newuser@example.com");
        Assert.assertEquals(1000, user.getCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals("Foo1", user.getFirstName());
        Assert.assertEquals("Bar2", user.getLastName());
        Assert.assertFalse(user.isEmailVerified());
        Assert.assertEquals(Role.ROLE_USER, user.getRole());

        Mockito.verify(mailSender).send(new EmailChangedNotificationEmail("user@example.com", "newuser@example.com"));
        Mockito.verify(mailSender).send(new EmailChangedVerificationEmail(user, getServerName()));
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUser', 'usertochangepassword@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 0, false, 0)"
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements =
                    "DELETE FROM user_login WHERE id = '42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d'"
            )
    })
    public void update_updatePassword() {
        var authToken = createAuthTokenFor("usertochangepassword@example.com");

        var updateRequest = putRequest("/user/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d",
                authToken,
                new UpdateUserRequest("usertochangepassword@example.com", "FooUser", "BarUser", "BrandNewPassword1", BigDecimal.valueOf(12)),
                Object.class
        );
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateRequest.getStatusCode());

        var user = getUserLoginRepository().getByEmail("usertochangepassword@example.com");
        Assert.assertTrue(getPasswordEncoder().matches("BrandNewPassword1", user.getPassword()));
        Mockito.verify(mailSender).send(new NewPasswordWasSetEmail(user));
    }

    @Test
    @SqlGroup(
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements =
                    "UPDATE user_login SET calories_per_day = 100, first_name = 'FooUser', last_name = 'BarUser', password = '$2a$10$21zhs.cyJGMLEMGvShqB4.EvqsKWhLwllPmrhkn.2kwYLlsywE8oe' WHERE email = 'user@example.com'"
            )
    )
    public void update_AdminIsUpdatingUserAccount() {
        var authToken = createAuthTokenFor("admin@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var updateRequest = putRequest("/user/" + userId,
                authToken,
                new UpdateUserRequest("user@example.com", "Foo1", "Bar2", null, BigDecimal.valueOf(1000)),
                ValidationFailure.class
        );
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateRequest.getStatusCode());

        var user = getUserLoginRepository().getByEmail("user@example.com");
        Assert.assertEquals(1000, user.getCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals("Foo1", user.getFirstName());
        Assert.assertEquals("Bar2", user.getLastName());
        Assert.assertEquals(Role.ROLE_USER, user.getRole());

        Mockito.verify(mailSender, Mockito.never()).send();
    }

    @Test
    @SqlGroup(
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements =
                    "UPDATE user_login SET calories_per_day = 0, first_name = 'FooUser', last_name = 'BarUser', password = '$2a$10$21zhs.cyJGMLEMGvShqB4.EvqsKWhLwllPmrhkn.2kwYLlsywE8oe' WHERE email = 'user@example.com'"
            )
    )
    public void update_UserIsUpdatingOwnAccount() {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var updateRequest = putRequest("/user/" + userId,
                authToken,
                new UpdateUserRequest("user@example.com", "Foo1", "Bar2", "Password12", BigDecimal.valueOf(1000)),
                ValidationFailure.class
        );
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateRequest.getStatusCode());

        UserLogin user = getUserLoginRepository().getByEmail("user@example.com");
        Assert.assertEquals(1000, user.getCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals("Foo1", user.getFirstName());
        Assert.assertEquals("Bar2", user.getLastName());
        Assert.assertTrue(getPasswordEncoder().matches("Password12", user.getPassword()));
        Assert.assertEquals(Role.ROLE_USER, user.getRole());

        Mockito.verify(mailSender).send(new NewPasswordWasSetEmail(user));
    }

    @Test
    public void delete_MissingPermission() {
        var authToken = createAuthTokenFor("user@example.com");

        var adminId = getUserLoginRepository().getByEmail("admin@example.com").getId();

        var deleteResponse = deleteRequest("/user/" + adminId, authToken, String.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, deleteResponse.getStatusCode());
    }

    @Test
    public void delete_UnknownUser() {
        var authToken = createAuthTokenFor("user@example.com");

        var deleteResponse = deleteRequest("/user/" + UUID.randomUUID(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, deleteResponse.getStatusCode());
        Assert.assertEquals("Requested entity can't be found. Probably wrong ID", deleteResponse.getBody());
    }

    @Test
    @SqlGroup(
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUser', 'usertodelete@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 0, false, 0)",
            }
        )
    )

    public void delete_UserSelfOK() {
        var authToken = createAuthTokenFor("usertodelete@example.com");

        var deleteResponse = deleteRequest("/user/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d", authToken, Object.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteResponse.getStatusCode());

        authToken = createAuthTokenFor("admin@example.com");

        var listMealRawResponse = getRequest("/user?page=0&size=2&sort=user.email,desc&filter=user.id eq '42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d'", authToken, UserListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        Assert.assertEquals(0, listMealRawResponse.getBody().getData().length);
    }

    @Test
    @SqlGroup(
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUser', 'usertodelete@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 5, true, 0)",
            }
            )

    )
    public void delete_AdminOtherOK() {
        var authToken = createAuthTokenFor("admin@example.com");

        var deleteResponse = deleteRequest("/user/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d", authToken, Object.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteResponse.getStatusCode());
    }

    @Test
    public void unlock_MissingPermission() {
        var authToken = createAuthTokenFor("user@example.com");

        var unlockResponse = postRequest("/user/unlock/" + UUID.randomUUID(), authToken, String.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, unlockResponse.getStatusCode());
    }

    @Test
    public void unlock_UnknownUser() {
        var authToken = createAuthTokenFor("admin@example.com");

        var unlockResponse = postRequest("/user/unlock/" + UUID.randomUUID(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, unlockResponse.getStatusCode());
        Assert.assertEquals("Requested entity can't be found. Probably wrong ID", unlockResponse.getBody());
    }

    @Test
    public void unlock_NotLocked() {
        var authToken = createAuthTokenFor("admin@example.com");

        var adminId = getUserLoginRepository().getByEmail("admin@example.com").getId();

        var unlockResponse = postRequest("/user/unlock/" + adminId, authToken, String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, unlockResponse.getStatusCode());
        Assert.assertEquals("User isn't locked" ,unlockResponse.getBody());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUser', 'locked.user@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 5, true, 0)",
            }
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = {
                    "DELETE FROM user_login WHERE email = 'locked.user@example.com'",
            })
    })
    public void unlock_OK() {

        var authToken = createAuthTokenFor("admin@example.com");

        var unlockResponse = postRequest("/user/unlock/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d", authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, unlockResponse.getStatusCode());
        Assert.assertFalse(getUserLoginRepository().getByEmail("admin@example.com").isLocked());
    }

    @Test
    public void invite_MissingPermission() {
        var authToken = createAuthTokenFor("user@example.com");

        var invitationResponse = postRequest("/user/invite", authToken, new InviteRequest("invitation@example.com"), Object.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, invitationResponse.getStatusCode());
    }

    @Test
    public void invite_UserAlreadyExists() {
        var authToken = createAuthTokenFor("admin@example.com");

        var invitationResponse = postRequest("/user/invite", authToken, new InviteRequest("user@example.com"), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, invitationResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("email", "UserLogin with this e-mail already exists.")), invitationResponse.getBody().getFieldErrors());
    }

    @Test
    public void invite_InvalidEmail() {
        var authToken = createAuthTokenFor("admin@example.com");

        var invitationResponse = postRequest("/user/invite", authToken, new InviteRequest("foobar"), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, invitationResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("email", "must be a well-formed email address")), invitationResponse.getBody().getFieldErrors());
    }

    @Test
    @SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM invitation_token WHERE email = 'newinviteduser@example.com'")
    })
    public void invite_OK() {
        var authToken = createAuthTokenFor("admin@example.com");

        var invitationResponse = postRequest("/user/invite", authToken, new InviteRequest("newinviteduser@example.com"), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, invitationResponse.getStatusCode());
        Assert.assertTrue(getInvitationTokenRepository().findByEmail("newinviteduser@example.com").isPresent());

        Mockito.verify(mailSender).send(new InvitationEmail(getInvitationTokenRepository().getByEmail("newinviteduser@example.com"), getServerName()));
    }

    @Test
    public void promoteToManager_MissingUser() {
        var authToken = createAuthTokenFor("admin@example.com");

        var promoteResponse = postRequest("/user/change-role/" + UUID.randomUUID(), authToken, new ChangeRoleRequest(Role.ROLE_MANAGER), String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, promoteResponse.getStatusCode());
        Assert.assertEquals("Requested entity can't be found. Probably wrong ID", promoteResponse.getBody());
    }

    @Test
    public void promoteToManager_MissingPermission() {
        var authToken = createAuthTokenFor("user@example.com");

        var adminId = getUserLoginRepository().getByEmail("admin@example.com").getId();

        var promoteResponse = postRequest("/user/change-role/" + adminId, authToken, new ChangeRoleRequest(Role.ROLE_MANAGER), Object.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, promoteResponse.getStatusCode());
    }

    @Test
    public void promoteToManagerAlreadyManager() {
        var authToken = createAuthTokenFor("admin@example.com");

        var managerId = getUserLoginRepository().getByEmail("manager@example.com").getId();

        var promoteResponse = postRequest("/user/change-role/" + managerId, authToken, new ChangeRoleRequest(Role.ROLE_MANAGER), String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, promoteResponse.getStatusCode());
        Assert.assertEquals("This role is already set", promoteResponse.getBody());
    }

    @Test
    public void promoteToAdmin_MissingPermission() {
        var authToken = createAuthTokenFor("manager@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var rawPromoteResponse = postRequest("/user/change-role/" + userId, authToken, new ChangeRoleRequest(Role.ROLE_ADMIN), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, rawPromoteResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("role", "You aren't eligible to set this role")), rawPromoteResponse.getBody().getFieldErrors());
    }

    @Test
    public void promoteUnableToPromoteSelf() {
        var authToken = createAuthTokenFor("admin@example.com");

        var adminId = getUserLoginRepository().getByEmail("admin@example.com").getId();

        var promoteResponse = postRequest("/user/change-role/" + adminId, authToken, new ChangeRoleRequest(Role.ROLE_MANAGER), String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, promoteResponse.getStatusCode());
        Assert.assertEquals("You can't change role on your own account", promoteResponse.getBody());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUser', 'to.promote.user@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 0, false, 0)",
                }
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = {
                    "DELETE FROM user_login WHERE email = 'to.promote.user@example.com'",
            })
    })
    public void promoteToAdmin_OK() {
        var authToken = createAuthTokenFor("admin@example.com");

        var promoteResponse = postRequest("/user/change-role/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d", authToken, new ChangeRoleRequest(Role.ROLE_ADMIN), String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, promoteResponse.getStatusCode());
        var newRole = getJdbc().queryForObject("SELECT role FROM user_login WHERE id = '42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d'", Role.class);
        Assert.assertEquals(Role.ROLE_ADMIN, newRole);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUser', 'to.promote.user@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 0, false, 0)",
            }
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = {
                    "DELETE FROM user_login WHERE email = 'to.promote.user@example.com'",
            })
    })
    public void promoteToManager_OK() {
        var authToken = createAuthTokenFor("manager@example.com");

        var promoteResponse = postRequest("/user/change-role/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d", authToken, new ChangeRoleRequest(Role.ROLE_MANAGER), String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, promoteResponse.getStatusCode());

        var newRole = getJdbc().queryForObject("SELECT role FROM user_login WHERE id = '42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d'", Role.class);
        Assert.assertEquals(Role.ROLE_MANAGER, newRole);
    }
}
