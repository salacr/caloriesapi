package org.salac.toptal.task2.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salac.toptal.task2.HumanReadableError;
import org.salac.toptal.task2.HumanReadableFieldError;
import org.salac.toptal.task2.ValidationFailure;
import org.salac.toptal.task2.controller.meal.MealRequest;
import org.salac.toptal.task2.dto.service.MealListResponse;
import org.salac.toptal.task2.dto.service.MealRow;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MealTest extends AbstractTest{

    @Test
    public void simpleListMealsByAdmin()
    {
        var authToken = createAuthTokenFor("admin@example.com");


        var listMealRawResponse = getRequest("/meal?page=1&size=2&sort=meal.date,desc", authToken, MealListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        var responseBody = listMealRawResponse.getBody();

        Assert.assertEquals(2, responseBody.getData().length );
        Assert.assertArrayEquals(new MealRow[]{
                new MealRow(UUID.fromString("ce79ec58-0ae9-485c-ad52-97a793a21831"), new BigDecimal("30.00"), LocalDate.parse("2018-01-03"), true, "ham and eggs", LocalTime.parse("11:15:00"), UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), new BigDecimal("30.00")),
                new MealRow(UUID.fromString("513c9024-3c98-4818-8e00-37f052e2517e"), new BigDecimal("20.00"), LocalDate.parse("2018-01-02"), true, "ham", LocalTime.parse("11:00:00"), UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), new BigDecimal("20.00")),
        }, responseBody.getData() );
        Assert.assertNull(responseBody.getCount());
        Assert.assertTrue(responseBody.isNext());

        listMealRawResponse = getRequest("/meal?page=0&size=10&sort=meal.time,asc", authToken, MealListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        responseBody = listMealRawResponse.getBody();

        Assert.assertEquals(5, responseBody.getData().length );
        Assert.assertArrayEquals(new MealRow[]{
                new MealRow(UUID.fromString("41c99109-2c28-49e2-9e20-ad8a7c029643"), new BigDecimal("10.00"), LocalDate.parse("2018-01-01"), true, "eggs", LocalTime.parse("10:00:00"), UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), new BigDecimal("10.00")),
                new MealRow(UUID.fromString("513c9024-3c98-4818-8e00-37f052e2517e"), new BigDecimal("20.00"), LocalDate.parse("2018-01-02"), true, "ham", LocalTime.parse("11:00:00"), UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), new BigDecimal("20.00")),
                new MealRow(UUID.fromString("ce79ec58-0ae9-485c-ad52-97a793a21831"), new BigDecimal("30.00"), LocalDate.parse("2018-01-03"), true, "ham and eggs", LocalTime.parse("11:15:00"), UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), new BigDecimal("30.00")),
                new MealRow(UUID.fromString("eb33c72d-26f5-404b-b99d-0f5ee796345d"), new BigDecimal("30.00"), LocalDate.parse("2018-01-04"), true, "cola", LocalTime.parse("11:30:00"), UUID.fromString("de600d24-ffea-42d3-bf13-1e911fcfecea"), new BigDecimal("30.00")),
                new MealRow(UUID.fromString("eb33c72d-26f5-404b-b99d-0f5ee796346d"), new BigDecimal("60.00"), LocalDate.parse("2018-01-05"), true, "2 cola", LocalTime.parse("11:35:00"), UUID.fromString("de600d24-ffea-42d3-bf13-1e911fcfecea"), new BigDecimal("60.00"))
        }, responseBody.getData() );
        Assert.assertNull(responseBody.getCount());
        Assert.assertFalse(responseBody.isNext());

        listMealRawResponse = getRequest("/meal?page=10&size=10&calcCount=true", authToken, MealListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        responseBody = listMealRawResponse.getBody();

        Assert.assertEquals(0, responseBody.getData().length );
        Assert.assertEquals(Long.valueOf(5), responseBody.getCount());
        Assert.assertFalse(responseBody.isNext());
    }

    @Test
    public void simpleListMealsByAdminWithoutNext()
    {
        var authToken = createAuthTokenFor("admin@example.com");


        var listMealRawResponse = getRequest("/meal?page=1&size=2&sort=meal.date,desc&checkNext=false", authToken, MealListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        var responseBody = listMealRawResponse.getBody();

        Assert.assertEquals(2, responseBody.getData().length );
        Assert.assertArrayEquals(new MealRow[]{
                new MealRow(UUID.fromString("ce79ec58-0ae9-485c-ad52-97a793a21831"), new BigDecimal("30.00"), LocalDate.parse("2018-01-03"), true, "ham and eggs", LocalTime.parse("11:15:00"), UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), new BigDecimal("30.00")),
                new MealRow(UUID.fromString("513c9024-3c98-4818-8e00-37f052e2517e"), new BigDecimal("20.00"), LocalDate.parse("2018-01-02"), true, "ham", LocalTime.parse("11:00:00"), UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), new BigDecimal("20.00")),
        }, responseBody.getData() );
        Assert.assertNull(responseBody.getCount());
        Assert.assertNull(responseBody.isNext());
    }

    @Test
    public void filteredListMealsByAdmin()
    {
        var authToken = createAuthTokenFor("admin@example.com");


        var listMealRawResponse = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.text eq 'ham'", authToken, MealListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        var responseBody = listMealRawResponse.getBody();

        Assert.assertEquals(1, responseBody.getData().length );
        Assert.assertArrayEquals(new MealRow[]{
                new MealRow(UUID.fromString("513c9024-3c98-4818-8e00-37f052e2517e"), new BigDecimal("20.00"), LocalDate.parse("2018-01-02"), true, "ham", LocalTime.parse("11:00:00"), UUID.fromString("a212df05-a3c7-4335-bb84-4fe8db0a1498"), new BigDecimal("20.00")),
        }, responseBody.getData() );
        Assert.assertNull(responseBody.getCount());
        Assert.assertFalse(responseBody.isNext());
    }

    @Test
    public void badFilterListMealsByAdmin()
    {
        var authToken = createAuthTokenFor("admin@example.com");


        var listMealRawResponse = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.text sasa 'ham'", authToken, String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, listMealRawResponse.getStatusCode() );
        Assert.assertEquals("Bad filter syntax:line 1:15 missing {'eq', 'ne', 'gt', 'lt'} at ''ham''", listMealRawResponse.getBody() );
    }

    @Test
    public void badSortingListMealsByAdmin()
    {
        var authToken = createAuthTokenFor("admin@example.com");


        var listMealRawResponse = getRequest("/meal?page=0&size=10&sort=mealsss.time,asc", authToken, String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, listMealRawResponse.getStatusCode() );
        Assert.assertEquals("You can't sort by this column, column in this reports are:meal.userid, meal.date, meal.isLessCaloriesThenExpected, meal.calories, meal.id, meal.time, meal.text", listMealRawResponse.getBody() );
    }

    @Test
    public void simpleListMealsByManager()
    {
        var authToken = createAuthTokenFor("manager@example.com");


        var listMealRawResponse = getRequest("/meal?page=0&size=1&sort=meal.date,desc&calcCount=true", authToken, MealListResponse.class);
        Assert.assertEquals(HttpStatus.OK, listMealRawResponse.getStatusCode() );
        var responseBody = listMealRawResponse.getBody();

        Assert.assertEquals(1, responseBody.getData().length );
        Assert.assertArrayEquals(new MealRow[]{
                new MealRow(UUID.fromString("eb33c72d-26f5-404b-b99d-0f5ee796346d"), new BigDecimal("60.00"), LocalDate.parse("2018-01-05"), true, "2 cola", LocalTime.parse("11:35:00"), UUID.fromString("de600d24-ffea-42d3-bf13-1e911fcfecea"), new BigDecimal("60.00"))
        }, responseBody.getData() );
        Assert.assertEquals(Long.valueOf(2), responseBody.getCount());
        Assert.assertTrue(responseBody.isNext());

    }


    @Test
    public void createMealSelfBadRequest()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, LocalDate.now(), LocalTime.now(), "", BigDecimal.valueOf(-5)), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, createMealResponse.getStatusCode());
        Assert.assertEquals(
            Set.of(
                new HumanReadableFieldError("text", "size must be between 1 and 255"),
                new HumanReadableFieldError("calories", "must be greater than or equal to 0")
            ),
            createMealResponse.getBody().getFieldErrors()
        );
    }

    @Test
    public void createMealSelfWithCalories()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(10)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());
        Assert.assertNotNull(createMealResponse.getBody());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];

        Assert.assertEquals(10, mealRow.getCalories().doubleValue(), 0);
        Assert.assertEquals("Eggs", mealRow.getText());
        Assert.assertEquals(userId, mealRow.getUser());
        Assert.assertEquals(nowDate.toLocalDate(), mealRow.getDate());
        Assert.assertEquals(nowDate.toLocalTime(), mealRow.getTime());

        var deleteRequest = deleteRequest("/meal/" + createMealResponse.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void createMealSelfWithoutCaloriesWithoutOverLimit()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "100 Eggs", null), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());
        Assert.assertEquals(7150, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals(7150, mealRow.getCalories().doubleValue(), 0);

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());

    }

    @Test
    public void createMealSelfWithoutCalories()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", null), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());
        Assert.assertEquals(71.5, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals(71.5, mealRow.getCalories().doubleValue(), 0);

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());

    }

    @Test
    public void createMealSelfOverLimit()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "100 Eggs", null), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());
        Assert.assertEquals(7150, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals(7150, mealRow.getCalories().doubleValue(), 0);

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void createMealSelfUnderLimit()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "1 Eggs", null), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.date gt '1900-01-01' AND meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());
        Assert.assertEquals(71.5, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals(71.5, mealRow.getCalories().doubleValue(), 0);

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void updateMealSelfChangeFoodWithoutCalories()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(10)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var updateMealResponse = putRequest("/meal/" + createMealResponse.getBody(), authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "10 Eggs", null), UUID.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());
        Assert.assertEquals(715, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals(715, mealRow.getCalories().doubleValue(), 0);

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void updateMealSelfChangeFoodWithCalories()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(10)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var updateMealResponse = putRequest("/meal/" + createMealResponse.getBody(), authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "10 Eggs", BigDecimal.valueOf(2)), UUID.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());
        Assert.assertEquals(2, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);
        Assert.assertEquals(2, mealRow.getCalories().doubleValue(), 0);

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void updateMealSelfChangeToOverLimit()
    {
        var authToken = createAuthTokenFor("user@example.com");
        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(10)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());


        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());
        Assert.assertEquals(10, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);

        var updateMealResponse = putRequest("/meal/" + createMealResponse.getBody(), authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "10 Eggs", BigDecimal.valueOf(2000)), UUID.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateMealResponse.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());
        Assert.assertEquals(2000, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void updateMealSelfChangeToUnderLimit()
    {
        var authToken = createAuthTokenFor("user@example.com");
        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(10000)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());


        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());
        Assert.assertEquals(10000, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);

        var updateMealResponse = putRequest("/meal/" + createMealResponse.getBody(), authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "10 Eggs", BigDecimal.valueOf(20)), UUID.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateMealResponse.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());
        Assert.assertEquals(20, mealRow.getConsumedCaloriesPerDay().doubleValue(), 0);

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    @SqlGroup(
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO meal (id, calories, date, text, time, user_login_id) VALUES" +
                            "('41c99109-2c28-49e2-9e20-ad8a7c02964a', 10, '2018-01-01', 'eggs', '10:00:00', 'a212df05-a3c7-4335-bb84-4fe8db0a1498')"
            )
    )
    public void deleteMealSelf()
    {
        var authToken = createAuthTokenFor("user@example.com");
        var deleteRequest = deleteRequest("/meal/41c99109-2c28-49e2-9e20-ad8a7c02964a", authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());

        Assert.assertFalse(getMealRepository().findById(UUID.fromString("41c99109-2c28-49e2-9e20-ad8a7c02964a")).isPresent());
    }

    @Test
    public void deleteMealSelfOverLimit()
    {
        var authToken = createAuthTokenFor("user@example.com");
        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(10000)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());
        var createMealResponse2 = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(10000)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());


        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());

        var deleteRequest = deleteRequest("/meal/" + createMealResponse.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse2.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());

        deleteRequest = deleteRequest("/meal/" + createMealResponse2.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void deleteMealSelfUnderLimit()
    {
        var authToken = createAuthTokenFor("user@example.com");
        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(600)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());
        var createMealResponse2 = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(600)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());


        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());

        var deleteRequest = deleteRequest("/meal/" + createMealResponse.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse2.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());

        deleteRequest = deleteRequest("/meal/" + createMealResponse2.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void testMultipleAdd()
    {
        var authToken = createAuthTokenFor("user@example.com");
        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(400)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());

        var createMealResponse2 = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(400)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());

        var createMealResponse3 = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Eggs", BigDecimal.valueOf(400)), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());


        var deleteRequest = deleteRequest("/meal/" + createMealResponse.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse3.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());


        var updateMealResponse = putRequest("/meal/" + createMealResponse2.getBody(), authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "10 Eggs", BigDecimal.valueOf(2000)), UUID.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateMealResponse.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse3.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertFalse(mealRow.isLessThenExpected());

        deleteRequest = deleteRequest("/meal/" + createMealResponse2.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse3.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());

        deleteRequest = deleteRequest("/meal/" + createMealResponse3.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void createMealOther()
    {
        var authToken = createAuthTokenFor("admin@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Ham", null), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());
        Assert.assertEquals(userId, mealRow.getUser());

        var deleteRequest = deleteRequest("/meal/" + mealRow.getId(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void createMealMissingPermission()
    {
        var authToken = createAuthTokenFor("manager@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Ham", null), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, createMealResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("userId", "You aren't eligible to create meal for that user")), createMealResponse.getBody().getFieldErrors());
    }

    @Test
    public void createMealMissingMeal()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "", null), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, createMealResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("text", "size must be between 1 and 255")), createMealResponse.getBody().getFieldErrors());
    }

    @Test
    public void createMealDateInFuture()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate().plusDays(2), nowDate.toLocalTime(), "dsdsdsds", null), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, createMealResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableError("You can't set future date")), createMealResponse.getBody().getObjectError());
    }

    @Test
    public void updateMealOther()
    {
        var authToken = createAuthTokenFor("admin@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "Ham", null), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertTrue(mealRow.isLessThenExpected());
        Assert.assertEquals(userId, mealRow.getUser());

        var updateMealResponse = putRequest("/meal/" + createMealResponse.getBody(), authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "10 Ham", null), UUID.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, updateMealResponse.getStatusCode());

        mealRow = getRequest("/meal?page=0&size=10&sort=meal.time,asc&filter=meal.id eq '" + createMealResponse.getBody() + "'", authToken, MealListResponse.class).getBody().getData()[0];
        Assert.assertEquals("10 Ham", mealRow.getText());

        var deleteRequest = deleteRequest("/meal/" + createMealResponse.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void updateMealMissingPermission()
    {
        var authToken = createAuthTokenFor("user@example.com");
        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = postRequest("/meal", authToken, new MealRequest(null, nowDate.toLocalDate(), nowDate.toLocalTime(), "100 Eggs", null), Object.class);
        Assert.assertEquals(HttpStatus.CREATED, createMealResponse.getStatusCode());

        var managerAuthToken = createAuthTokenFor("manager@example.com");
        var updateMealResponse = putRequest("/meal/" + createMealResponse.getBody(), managerAuthToken, new MealRequest(null, nowDate.toLocalDate(), nowDate.toLocalTime(), "10 Ham", null), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, updateMealResponse.getStatusCode());

        var deleteRequest = deleteRequest("/meal/" + createMealResponse.getBody(), authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());
    }

    @Test
    public void updateMealMissingMeal()
    {
        var authToken = createAuthTokenFor("user@example.com");

        var userId = getUserLoginRepository().getByEmail("user@example.com").getId();

        var nowDate = LocalDateTime.now().withNano(0);

        var createMealResponse = putRequest("/meal/41c99109-2c28-49e2-9e20-ad8a7c029643" , authToken, new MealRequest(userId, nowDate.toLocalDate(), nowDate.toLocalTime(), "", null), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, createMealResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("text", "size must be between 1 and 255")), createMealResponse.getBody().getFieldErrors());
    }

    @Test
    @SqlGroup(
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO meal (id, calories, date, text, time, user_login_id) VALUES" +
                    "('41c991c9-2c28-49e2-9e20-ad8a7c02964a', 10, '2018-01-01', 'eggs', '10:00:00', 'a212df05-a3c7-4335-bb84-4fe8db0a1498')"
            )
    )
    public void deleteMealOther()
    {
        var authToken = createAuthTokenFor("admin@example.com");
        var deleteRequest = deleteRequest("/meal/41c991c9-2c28-49e2-9e20-ad8a7c02964a", authToken, String.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, deleteRequest.getStatusCode());

        Assert.assertFalse(getMealRepository().findById(UUID.fromString("41c991c9-2c28-49e2-9e20-ad8a7c02964a")).isPresent());
    }

    @Test
    public void deleteMealMissingMeal()
    {
        var authToken = createAuthTokenFor("user@example.com");
        var deleteRequest = deleteRequest("/meal/5e1398d9-cb98-4db4-8d16-547bea8874b9", authToken, String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, deleteRequest.getStatusCode());
    }

    @Test
    public void deleteMealMissingPermission()
    {
        var authToken = createAuthTokenFor("manager@example.com");
        var deleteRequest = deleteRequest("/meal/41c99109-2c28-49e2-9e20-ad8a7c029643", authToken, Object.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, deleteRequest.getStatusCode());
    }
}
