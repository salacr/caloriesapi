package org.salac.toptal.task2.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.salac.toptal.task2.HumanReadableFieldError;
import org.salac.toptal.task2.ValidationFailure;
import org.salac.toptal.task2.controller.auth.*;
import org.salac.toptal.task2.dto.Role;
import org.salac.toptal.task2.mail.NewPasswordWasSetEmail;
import org.salac.toptal.task2.mail.PasswordResetEmail;
import org.salac.toptal.task2.mail.VerificationEmail;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.*;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AuthTest extends AbstractTest{

    @SpyBean
    private MailSender mailSender;

    @Before
    public void setupMock() {
        doNothing().when(mailSender).send(any(SimpleMailMessage.class));
    }

    @Test
    public void signIn_MissingNameAndSurnameAndNegativeCalories() {
        var signInResponse = postRequest("/auth/sign-in", new SignInRequest(null, "", "test@example.com", "Secret/Password1", new BigDecimal("-1")), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, signInResponse.getStatusCode());
        Assert.assertEquals(
                Set.of(
                        new HumanReadableFieldError("firstName", "must not be null"),
                        new HumanReadableFieldError("lastName", "size must be between 1 and 255"),
                        new HumanReadableFieldError("caloriesPerDay", "must be greater than or equal to 0")
                ),
                signInResponse.getBody().getFieldErrors()
        );
    }

    @Test
    public void signIn_InvalidEmail() {
        var signInResponse = postRequest("/auth/sign-in", new SignInRequest("Foo", "Bar", "test", "Secret/Password1", BigDecimal.ONE), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, signInResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("email", "must be a well-formed email address")), signInResponse.getBody().getFieldErrors());
    }

    @Test
    public void signIn_WeakPassword() {
        var signInResponse = postRequest("/auth/sign-in", new SignInRequest("Foo", "Bar", "test@example.com", "123456", BigDecimal.ONE), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, signInResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("password", "Password must contain at least 8 characters, including 1 uppercase letter, 1 lowercase letter, and 1 number.")), signInResponse.getBody().getFieldErrors());
    }

    @Test
    public void signIn_ExistingUserAndMissingCallories() {
        var signInResponse = postRequest("/auth/sign-in", new SignInRequest("Foo", "Bar", "admin@example.com", "Secret/Password1", null), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, signInResponse.getStatusCode());
        Assert.assertEquals(
                Set.of(
                        new HumanReadableFieldError("email", "UserLogin with this e-mail already exists."),
                        new HumanReadableFieldError("caloriesPerDay", "must not be null")
                ),
                signInResponse.getBody().getFieldErrors()
        );
    }

    @Test
    @Transactional
    @SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM email_verification_token WHERE user_login_id = (SELECT id FROM user_login WHERE email = 'new-user-3@example.com')"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM user_login WHERE email = 'new-user-3@example.com'")
    })
    public void signIn_OK() {
        var signInResponse = postRequest("/auth/sign-in", new SignInRequest("Foo", "Bar", "new-user-3@example.com", "Secret/Password1", BigDecimal.ONE), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, signInResponse.getStatusCode());
        Assert.assertNotNull(signInResponse.getBody());

        var user = getUserLoginRepository().getByEmail("new-user-3@example.com");
        Assert.assertEquals(new BigDecimal("1.00"), user.getCaloriesPerDay());
        Assert.assertEquals("Foo", user.getFirstName());
        Assert.assertEquals("Bar", user.getLastName());
        Assert.assertEquals("new-user-3@example.com", user.getEmail());
        Assert.assertFalse(user.isEmailVerified());
        Assert.assertFalse(user.isLocked());
        Assert.assertEquals(0, user.getBadLoginAtemptsInRow());


        Mockito.verify(mailSender).send(new VerificationEmail(getUserLoginRepository().getByEmail("new-user-3@example.com"), getServerName()));
    }


    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO invitation_token (id, email) VALUES " +
                            "('92ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'invitation@example.com')"
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM invitation_token WHERE id = '92ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d'")
    })
    public void fromInvitation_MissingNameAndSurname() {
        var fromInvitationResponse = postRequest("/auth/from-invitation/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d", new InvitationRequest(null, "", "Secret/Password1", BigDecimal.ONE), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, fromInvitationResponse.getStatusCode());
        Assert.assertEquals(Set.of(new HumanReadableFieldError("firstName", "must not be null"), new HumanReadableFieldError("lastName", "size must be between 1 and 255")), fromInvitationResponse.getBody().getFieldErrors());
    }


    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO invitation_token (id, email) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'invitation@example.com')"
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM invitation_token WHERE id = '42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d'")
    })
    public void fromInvitation_WeakPassword() {
        var fromInvitationResponse = postRequest("/auth/from-invitation/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d" , new InvitationRequest("Foo", "Bar", "Secret", null), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, fromInvitationResponse.getStatusCode());
        Assert.assertEquals(
                Set.of(
                    new HumanReadableFieldError("password", "Password must contain at least 8 characters, including 1 uppercase letter, 1 lowercase letter, and 1 number."),
                    new HumanReadableFieldError("caloriesPerDay", "must not be null")
                ),
                fromInvitationResponse.getBody().getFieldErrors()
        );
    }

    @Test
    public void fromInvitation_ExpiredOrBadToken() {
        var fromInvitationResponse = postRequest("/auth/from-invitation/" + UUID.randomUUID(), new InvitationRequest("Foo", "Bar", "Secret", BigDecimal.ZERO), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, fromInvitationResponse.getStatusCode());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO invitation_token (id, email) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'invitation@example.com')"
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = {
                    "DELETE FROM user_login WHERE email = 'invitation@example.com'",
            })
    })
    public void fromInvitation_OK() {
        var fromInvitationResponse = postRequest("/auth/from-invitation/42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d", new InvitationRequest("Foo", "Bar", "SecretPassword1", BigDecimal.ONE), UUID.class);
        Assert.assertEquals(HttpStatus.CREATED, fromInvitationResponse.getStatusCode());
        Assert.assertNotNull(fromInvitationResponse.getBody());
        Mockito.verify(mailSender, Mockito.times(0)).send();

        var userLogin = getUserLoginRepository().getByEmail("invitation@example.com");
        Assert.assertEquals(Role.ROLE_USER, userLogin.getRole());
        Assert.assertEquals("Foo", userLogin.getFirstName());
        Assert.assertEquals("Bar", userLogin.getLastName());
        Assert.assertEquals(new BigDecimal("1.00"), userLogin.getCaloriesPerDay());
        Assert.assertTrue(getPasswordEncoder().matches( "SecretPassword1", userLogin.getPassword()));

    }

    @Test
    public void verifyEmail_InvalidToken() {
        var verifyEmailResponse = getRequest("/auth/verify-email/" + UUID.randomUUID(), new InvitationRequest("Foo", "Bar", "Secret", BigDecimal.ONE), String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, verifyEmailResponse.getStatusCode());
        Assert.assertEquals("Requested entity can't be found. Probably wrong ID", verifyEmailResponse.getBody());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUSer', 'locked.user@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 2, false, 100)",
                    "INSERT INTO email_verification_token (id, user_login_id) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cfdd', '42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d')"
            }
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = {
                    "DELETE FROM user_login WHERE email = 'locked.user@example.com'",
            })
    })
    public void verifyEmail_OK() {
        var verifyEmailResponse = getRequest("/auth/verify-email/42ea8ef0-a222-4e77-ba15-f4e2c4e7cfdd", null, ValidationFailure.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, verifyEmailResponse.getStatusCode());
    }


    @Test
    public void login_MissingUser() {
        var loginResponse = postRequest("/auth/login", new LogInRequest("mising@example.com", "BadPassword"), String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, loginResponse.getStatusCode());
        Assert.assertEquals("Requested entity can't be found. Probably wrong ID", loginResponse.getBody());
    }

    @Test
    public void login_BadPassword() {
        var loginResponse = postRequest("/auth/login", new LogInRequest("user@example.com", "BadPassword"), LogInResponse.class);
        Assert.assertEquals(HttpStatus.UNAUTHORIZED, loginResponse.getStatusCode());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUSer', 'locked.user@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 2, false, 100)"
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM user_login WHERE email = 'locked.user@example.com'")
    })
    public void login_LockUser() {
        var loginResponse = postRequest("/auth/login", new LogInRequest("locked.user@example.com", "BadPassword"), LogInResponse.class);
        Assert.assertEquals(HttpStatus.UNAUTHORIZED, loginResponse.getStatusCode());

        // no we tests that the user is locked
        loginResponse = postRequest("/auth/login", new LogInRequest("locked.user@example.com", "userLogin@example.com"), LogInResponse.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, loginResponse.getStatusCode());
        Assert.assertEquals("Account is locked please contact administrator or manager", loginResponse.getBody().getMessage());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = 
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                    "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUSer', 'locked-user@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', true, 4, true, 100)"
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM user_login WHERE email = 'locked-user@example.com'")
    })
    public void login_LockedUser() {
        var loginResponse = postRequest("/auth/login", new LogInRequest("locked-user@example.com", "userLogin@example.com"), LogInResponse.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, loginResponse.getStatusCode());
        Assert.assertEquals("Account is locked please contact administrator or manager", loginResponse.getBody().getMessage());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('42ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUSer', 'notverified@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', false, 0, false, 100)"
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM user_login WHERE email = 'notverified@example.com'")
    })
    public void login_emailNotVerified() {
        var loginResponse = postRequest("/auth/login", new LogInRequest("notverified@example.com", "userLogin@example.com"), LogInResponse.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, loginResponse.getStatusCode());
        Assert.assertEquals("E-mail isn't verified", loginResponse.getBody().getMessage());
    }

    @Test
    @Transactional
    public void login_OK() {
        var loginResponse = postRequest("/auth/login", new LogInRequest("user@example.com", "user@example.com"), LogInResponse.class);
        Assert.assertEquals(HttpStatus.OK, loginResponse.getStatusCode());
        Assert.assertTrue(loginResponse.getBody().getJwt().length() > 0);
        Assert.assertNotNull(loginResponse.getBody().getRefreshToken());

        var refreshTokenRepository = getRefreshTokenRepository();

        var refreshTokenOptional = refreshTokenRepository.findById(loginResponse.getBody().getRefreshToken());
        Assert.assertTrue(refreshTokenOptional.isPresent());
        var refreshToken = refreshTokenOptional.get();
        Assert.assertEquals("user@example.com", refreshToken.getUserLogin().getEmail());
        refreshTokenRepository.delete(refreshToken);
        refreshTokenRepository.flush();

    }

    @Test
    public void logout_BadToken() {
        var logoutResponse = postRequest("/auth/logout/" + UUID.randomUUID(), null, String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, logoutResponse.getStatusCode());
        Assert.assertEquals("Requested entity can't be found. Probably wrong ID", logoutResponse.getBody());
    }

    @Test
    public void logout_OK() {
        var rawLoginResponse = postRequest("/auth/login", new LogInRequest("user@example.com", "user@example.com"), LogInResponse.class);
        var loginResponse = rawLoginResponse.getBody();

        var logoutResponse = postRequest("/auth/logout/" + loginResponse.getRefreshToken(), null, Object.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, logoutResponse.getStatusCode());
        Assert.assertFalse(getRefreshTokenRepository().findById(loginResponse.getRefreshToken()).isPresent());
    }

    @Test
    public void refresh_BadToken() {
        var refreshResponse = postRequest("/auth/refresh/" + UUID.randomUUID(), null, String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, refreshResponse.getStatusCode());
        Assert.assertEquals("Requested entity can't be found. Probably wrong ID", refreshResponse.getBody());
    }

    @Test
    public void refresh_WrongToken() {
        var refreshResponse = postRequest("/auth/refresh/wrongtoken", null, String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, refreshResponse.getStatusCode());
    }

    @Test
    public void refresh_OK() {
        var rawLoginResponse = postRequest("/auth/login", new LogInRequest("user@example.com", "user@example.com"), LogInResponse.class);
        var loginResponse = rawLoginResponse.getBody();

        try {
            Thread.sleep(2000); // we must wait so we are sure that tokens are different
        }
        catch (Exception e)
        {}

        var refreshId = loginResponse.getRefreshToken();
        var refreshResponse = postRequest("/auth/refresh/" + refreshId, null, RefreshResponse.class);
        Assert.assertNotEquals(loginResponse.getJwt(), refreshResponse.getBody().getJwt());

        var refreshTokenRepository = getRefreshTokenRepository();
        refreshTokenRepository.deleteById(refreshId);
        refreshTokenRepository.flush();
    }

    @Test
    public void lostPasswordTestWrongEmail()
    {
        var lostPasswordRequest = postRequest("/auth/lost-password", new LostPasswordRequest("sssss"), ValidationFailure.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, lostPasswordRequest.getStatusCode());
    }

    @Test
    public void lostPasswordTestMissingEmail()
    {
        var lostPasswordRequest = postRequest("/auth/lost-password", new LostPasswordRequest("nobody@example.com"), String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, lostPasswordRequest.getStatusCode());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM lost_password_token WHERE user_login_id = (SELECT id FROM user_login WHERE email = 'admin@example.com')"),
    })
    public void lostPasswordTest()
    {
        var lostPasswordRequest = postRequest("/auth/lost-password", new LostPasswordRequest("admin@example.com"), Object.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, lostPasswordRequest.getStatusCode());

        var token = getLostPasswordRepository().findAll().get(0);

        Mockito.verify(mailSender).send(new PasswordResetEmail(token, getServerName()));
    }


    @Test
    public void newPasswordTestWrongToken()
    {
        var newPasswordResponse = postRequest("/auth/new-password/" + UUID.randomUUID(), new NewPasswordRequest("newPassword1"), String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, newPasswordResponse.getStatusCode());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements =
                    "INSERT INTO user_login (id, first_name, last_name, email, password, role, email_verified, bad_login_atempts_in_row, is_locked, calories_per_day) VALUES " +
                            "('22ea8ef0-a222-4e77-ba15-f4e2c4e7cf3d', 'FooUser', 'BarUSer', 'newpassword@example.com', '$2a$10$YXKfkrQbeHc/m4EN016dye9NfjuaZafvsRAmnMB6YFiywDTPMrqFK', 'ROLE_USER', false, 0, false, 100)"
            ),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM user_login WHERE email = 'newpassword@example.com'"),
    })
    public void newPasswordTest()
    {
        postRequest("/auth/lost-password", new LostPasswordRequest("newpassword@example.com"), Object.class);
        var token = getLostPasswordRepository().findAll().get(0);

        var newPasswordResponse = postRequest("/auth/new-password/" + token.getId(), new NewPasswordRequest("newPassword1"), Object.class);
        Assert.assertEquals(HttpStatus.NO_CONTENT, newPasswordResponse.getStatusCode());

        var user = getUserLoginRepository().getByEmail("newpassword@example.com");
        Assert.assertTrue(getPasswordEncoder().matches("newPassword1", user.getPassword()));
        Mockito.verify(mailSender).send(new NewPasswordWasSetEmail(user));
    }
}
