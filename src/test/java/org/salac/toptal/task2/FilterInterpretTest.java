package org.salac.toptal.task2;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.salac.toptal.task2.filterParser.FilterInterpret;
import org.salac.toptal.task2.filterParser.FilterParseException;
import org.salac.toptal.task2.filterParser.QueryType;
import org.salac.toptal.task2.filterParser.SQLFragment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;


public class FilterInterpretTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();


    private SQLFragment parse(QueryType queryType, String input)
    {

       var parser = new FilterInterpret();
       return parser.interpret(input, queryType);
    }

    @Test
    public void emptyInputTest()
    {
        SQLFragment result = parse(QueryType.USER, "");
        Assert.assertEquals("", result.getSql());
        Assert.assertEquals(0, result.getBinds().size());

    }

    @Test
    public void booleanOnlyColumnTest()
    {
        thrown.expect(FilterParseException.class);
        thrown.expectMessage("Bad filter syntax:line 1:13 mismatched input '<EOF>' expecting {'eq', 'ne'}");
        SQLFragment result = parse(QueryType.USER, "user.isLocked");
    }

    @Test
    public void booleanWrongComparatorTest()
    {
        thrown.expect(FilterParseException.class);
        thrown.expectMessage("Bad filter syntax:line 1:14 mismatched input 'lt' expecting {'eq', 'ne'}");
        SQLFragment result = parse(QueryType.USER, "user.isLocked lt true");
    }

    @Test
    public void simpleBooleanTest()
    {
        thrown.expect(FilterParseException.class);
        thrown.expectMessage("Bad filter syntax:line 1:13 mismatched input '<EOF>' expecting {'eq', 'ne'}");
        SQLFragment result = parse(QueryType.USER, "user.isLocked");
    }

    @Test
    public void testAllUserFields()
    {
        SQLFragment result = parse(QueryType.USER, "(user.email ne 'foobar@example.com' AND user.id eq '6bb7871e-0444-4b33-a217-bf05b4d38b1f') OR user.isLocked eq user.emailVerified " +
                "AND (user.firstName ne user.lastName AND user.role ne 'test' AND user.badPasswordsInRow lt 5 AND (user.calloriesPerDay gt 4))");
        Assert.assertEquals("( user_login.email != ? AND user_login.id = ? ) OR user_login.is_locked = user_login.email_verified AND ( user_login.first_name != user_login.last_name AND user_login.role != ? AND user_login.bad_login_atempts_in_row < ? AND ( user_login.calories_per_day > ? ) )", result.getSql());
        Assert.assertEquals(List.of("foobar@example.com", UUID.fromString("6bb7871e-0444-4b33-a217-bf05b4d38b1f"), "test", new BigDecimal("5"), new BigDecimal("4")), result.getBinds());
    }

    @Test
    public void testAllMealFields()
    {
        SQLFragment result = parse(QueryType.MEAL, "meal.date eq '1234-05-05' OR (meal.time eq '12:12:12') AND meal.calories gt 47 AND meal.text eq 'test' " +
                "OR meal.isLessCaloriesThenExpected eq false AND meal.id eq '6bb7871e-0444-4b33-a217-bf05b4d38b1f' AND meal.userid ne '6bb7871e-0444-4b33-a217-bf05b4d38b1f'");
        Assert.assertEquals("meal.date = ? OR ( meal.time = ? ) AND meal.calories > ? AND meal.text = ? OR meal.is_less_then_expected = false AND meal.id = ? AND meal.user_login_id != ?", result.getSql());



        var expected = new LinkedList<>();
        expected.add(LocalDate.parse("1234-05-05"));
        expected.add(LocalTime.parse("12:12:12"));
        expected.add(new BigDecimal("47"));
        expected.add("test");
        expected.add(UUID.fromString("6bb7871e-0444-4b33-a217-bf05b4d38b1f"));
        expected.add(UUID.fromString("6bb7871e-0444-4b33-a217-bf05b4d38b1f"));

        List<Object> binds = result.getBinds();
        Assert.assertEquals(expected.size(), binds.size());
        for (int i = 0; i < expected.size(); i++)
        {
            Assert.assertEquals(expected.get(i), result.getBinds().get(i));
        }
    }

    @Test
    public void simpleDateTest()
    {
        SQLFragment result = parse(QueryType.MEAL, "meal.date eq '2018-01-01'");
        Assert.assertEquals("meal.date = ?", result.getSql());
        Assert.assertEquals(List.of(LocalDate.parse("2018-01-01")), result.getBinds());
    }

    @Test
    public void simpleTimeTest()
    {
        SQLFragment result = parse(QueryType.MEAL, "meal.time gt '12:12:12'");
        Assert.assertEquals("meal.time > ?", result.getSql());
        Assert.assertEquals(List.of(LocalTime.parse("12:12:12")), result.getBinds());
    }

    @Test
    public void wrongDateFormatTest()
    {
        thrown.expect(FilterParseException.class);
        thrown.expectMessage("Wrong value 'sasasa' expecting date in format 'yyyy-mm-dd'");
        SQLFragment result = parse(QueryType.MEAL, "meal.date gt 'sasasa'");
    }

    @Test
    public void wrongTimeFormatTest()
    {
        thrown.expect(FilterParseException.class);
        thrown.expectMessage("Wrong value 'sasasa' expecting time in format 'hh:mm:ss'");
        SQLFragment result = parse(QueryType.MEAL, "meal.time gt 'sasasa'");
    }

    @Test
    public void wrongUUIDFormatTest()
    {
        thrown.expect(FilterParseException.class);
        thrown.expectMessage("Wrong value 'sasasa' expecting UUIDv4 format");
        SQLFragment result = parse(QueryType.MEAL, "meal.id eq 'sasasa'");
    }

    @Test
    public void simpleStringTest()
    {
        SQLFragment result = parse(QueryType.USER, "user.firstName ne 'Foo'");
        Assert.assertEquals("user_login.first_name != ?", result.getSql());
        Assert.assertEquals(List.of("Foo"), result.getBinds());
    }

    @Test
    public void simpleStringWithComaTest()
    {
        SQLFragment result = parse(QueryType.USER, "user.firstName ne 'Foo\\'dsds'");
        Assert.assertEquals("user_login.first_name != ?", result.getSql());
        Assert.assertEquals(List.of("Foo\\'dsds"), result.getBinds());
    }

    @Test
    public void simpleNumericTest()
    {
        SQLFragment result = parse(QueryType.MEAL, "meal.calories lt 5");
        Assert.assertEquals("meal.calories < ?", result.getSql());
        Assert.assertEquals(List.of(new BigDecimal("5")), result.getBinds());
    }
}
