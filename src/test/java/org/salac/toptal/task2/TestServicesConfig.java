package org.salac.toptal.task2;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.MailSender;

@Profile("test")
@Configuration
public class TestServicesConfig extends ServicesConfig{

    @Bean
    @Primary
    public MailSender mailSenderSpy() {
        MailSender mock = Mockito.mock(MailSender.class);
        return Mockito.spy(mock);
    }

}
