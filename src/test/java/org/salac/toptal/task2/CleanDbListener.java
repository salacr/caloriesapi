package org.salac.toptal.task2;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;

@Component
public class CleanDbListener implements TestExecutionListener {

    @Autowired
    private Flyway flyWay;

    public void beforeTestClass(TestContext testContext) {
        testContext.getApplicationContext()
                .getAutowireCapableBeanFactory()
                .autowireBean(this);

        flyWay.clean();
        flyWay.migrate();
    }

}
